﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class HUD : MonoBehaviour
{
    public static HUD Instance;

    private void Awake()
    {
        Instance = this;
    }

    public void ResetLevel()
    {
        SceneManager.LoadScene("Match");
    }

    public void GoToMenu()
    {
        SceneManager.LoadScene("Menu");
    }

}
