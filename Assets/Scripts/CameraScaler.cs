﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
[RequireComponent(typeof(Camera))]
public class CameraScaler : MonoBehaviour
{

    private Camera ortographicCamera;

    void Awake()
    {
        ortographicCamera = GetComponent<Camera>();

        UpdateSize();
    }

    void Update()
    {
        UpdateSize();
    }

    void UpdateSize()
    {
        var safeArea = Screen.safeArea;

        double baseAspect = 640.0f / 360.0f;
        double safeAspect = safeArea.width / safeArea.height;

        if (safeAspect < baseAspect)
        {
            float designWidth = 640.0f;
            float width = designWidth * Screen.width / safeArea.width;

            ortographicCamera.orthographicSize = width / 2.0f / ortographicCamera.aspect;
        }
        else
        {
            float designHeight = 360.0f;
            float height = designHeight * Screen.height / safeArea.height;

            ortographicCamera.orthographicSize = height / 2.0f;
        }

        float conv = ortographicCamera.orthographicSize * ortographicCamera.aspect * 2.0f;

        float dx = (0.5f - safeArea.center.x / Screen.width) * conv;
        float dy = (0.5f - safeArea.center.y / Screen.height) * conv;

        transform.localPosition = new Vector3(dx, dy, transform.localPosition.z);
    }
}
