﻿using UnityEngine;
using UnityEngine.UI;

public class Medal : MonoBehaviour
{

    public Image Image;

    public Sprite GoldSprite;
    public Sprite SilverSprite;
    public Sprite BronzeSprite;

    private float animationValue = 0.0f;
    private int currentRank = 4;

    private void Awake()
    {
        Image.gameObject.SetActive(false);
    }

    public void SetRank(int rank)
    {
        if(rank == currentRank)
        {
            return;
        }

        currentRank = rank;

        if(currentRank == 4)
        {
            Image.gameObject.SetActive(false);
        } else
        {
            Image.gameObject.SetActive(true);
            animationValue = 0.0f;

            if(currentRank == 1)
            {
                Image.sprite = GoldSprite;
            } else if(currentRank == 2)
            {
                Image.sprite = SilverSprite;
            } else
            {
                Image.sprite = BronzeSprite;
            }

            UpdateScale();
        }
    }

    private void Update()
    {
        animationValue += Time.deltaTime * 2.0f;
        animationValue = Mathf.Min(animationValue, 1.0f);

        UpdateScale();
    }

    void UpdateScale()
    {
        float scale = Easing.Back.Out(animationValue);
        transform.localScale = new Vector3(scale, scale, 1.0f);
    }

}
