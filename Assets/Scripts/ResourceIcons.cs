﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceIcons : MonoBehaviour
{
    public Sprite city;
    public Sprite coal;
    public Sprite crown;
    public Sprite fish;
    public Sprite gems;
    public Sprite gold;
    public Sprite grain;
    public Sprite horses;
    public Sprite ice;
    public Sprite iron;
    public Sprite stones;
    public Sprite tree;
    public Sprite volcano;
    public Sprite water;

    public Sprite GetById(string resourceId)
    {
        switch(resourceId)
        {
            case "city": return city;
            case "coal": return coal;
            case "crown": return crown;
            case "fish": return fish;
            case "gems": return gems;
            case "gold": return gold;
            case "grain": return grain;
            case "horses": return horses;
            case "ice": return ice;
            case "iron": return iron;
            case "stones": return stones;
            case "tree": return tree;
            case "volcano": return volcano;
            case "water": return water;
        }

        return null;
    }

    public Sprite GetByType(ResourceType resourceType)
    {
        switch(resourceType)
        {
            case ResourceType.Wood:
                return tree;
            case ResourceType.Gems:
                return gems;
            case ResourceType.Gold:
                return gold;
            case ResourceType.Horses:
                return horses;
            case ResourceType.Iron:
                return iron;
            default:
                Debug.LogError("Unknown resource type " + resourceType);
                return null;
        }
    }

    public Sprite GetByTokenType(TokenType resourceType)
    {
        switch (resourceType)
        {
            case TokenType.Wood:
                return tree;
            case TokenType.Gems:
                return gems;
            case TokenType.Gold:
                return gold;
            case TokenType.Horses:
                return horses;
            case TokenType.Iron:
                return iron;
            default:
                return null;
        }
    }
}
