﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ModePanel : MonoBehaviour
{
    public Button Button;
    public TextMeshProUGUI ButtonText;

    public void Setup(int coins, int min)
    {
        Button.interactable = coins >= min;
    }

}
