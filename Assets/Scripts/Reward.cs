﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Reward : MonoBehaviour
{
    public Image Image;
    public TextMeshProUGUI NameText;
    public GameObject RewardHeader;
    public UnitPortraits Portraits;
    public GameObject RewardParent;
    public TextMeshProUGUI DamageText;
    public TextMeshProUGUI HealthText;
    public GameObject NoRewardParent;

    void Start()
    {
        var save = SaveStorage.Load();

        var unlockedId = UnitUnlocked.LastUnlocked(save.Wins);

        var players = AppMain.Instance.SummaryParams.Players;

        var playerWon = players[0].PlayerId == 3 && players[0].Reward > players[1].Reward;

        if (unlockedId == null || !playerWon)
        {
            RewardParent.SetActive(false);
        } else
        {
            NoRewardParent.SetActive(false);
            Image.sprite = Portraits.GetForId(unlockedId);
            NameText.text = UnitConfig.Names[unlockedId];
            var cfg = UnitConfig.GetForId(unlockedId);

            DamageText.text = cfg.damage.ToString();
            HealthText.text = cfg.hp.ToString();
        }
    }
}
