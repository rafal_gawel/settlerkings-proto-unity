﻿using System;

[Serializable]
public class GameSave
{
    public int Coins;
    public bool SeenTutorial;
    public int Wins;
    public int PlayedGames;

    public GameSave(int coins, bool seenTutorial, int wins, int playedGames)
    {
        this.Coins = coins;
        this.SeenTutorial = seenTutorial;
        this.Wins = wins;
        this.PlayedGames = playedGames;
    }
}
