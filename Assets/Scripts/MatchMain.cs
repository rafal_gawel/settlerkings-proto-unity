﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MatchMain : MonoBehaviour
{

    public static MatchMain Instance;

    public PointsCalc PointsCalc { get; } = new PointsCalc();
    public bool MatchOver { get; private set; } = false;

    public HUD HUD;
    public Deck Deck;
    public Board Board;
    public Avatar[] Avatars;
    public Player[] Players;
    public Human Human;
    public Bot Bot;
    public ForgePanel[] BotForgePanels;
    public ForgePanel[] ForgePanels;

    public StandbyFighter StandbyFighterPrefab;
    readonly List<string> humanFighters = new List<string>();
    public float HumanFighterSpan { get; private set; } = 0.0f;

    readonly List<string> botFighters = new List<string>();
    public float BotFighterSpan { get; private set; } = 0.0f;

    private List<string> playerDeck;
    private List<string> botDeck;

    public int CurrentPlayerIndex { get; private set; }
    int FinishedMoves;

    public float TimeForTurnNormalized { get => timeForPlaceBlock / TIME_FOR_TURN; }

    const float TIME_FOR_TURN = 25.0f;
    float timeForPlaceBlock;
    bool timerRunning = false;

    public int DaysToBattle
    {
        get
        {
            return Mathf.Max(0, 8 - CurrentDay);
        }
    }

    public int CurrentDay { get; private set; } = 1;

    public readonly List<UnitForge> PlayerForges = new List<UnitForge>();
    public readonly List<UnitForge> BotForges = new List<UnitForge>();

    public List<UnitForge> UnasignedPlayerForges { get; private set; } = new List<UnitForge>();
    public List<UnitForge> UnasignedBotForges { get; private set; } = new List<UnitForge>();

    private void Awake()
    {
        Instance = this;

        FinishedMoves = 0;
        CurrentPlayerIndex = Random.Range(0, 2) == 0 ? 1 : 3;

        var matchParams = AppMain.Instance.MatchParams;

        playerDeck = MakeDeck(matchParams.PlayerUnitsIds);
        botDeck = MakeDeck(matchParams.OpponentUnitsIds);


        for (int i=0; i<4; i++)
        {
            AddPlayerForge();
        }

        for (int i = 0; i < 4; i++)
        {
            AddBotForge();
        }
    }

    List<string> MakeDeck(string[] unitIds)
    {
        List<string> deck = new List<string>();

        for(int i=0; i<2; i++)
        {
            for(int j=0; j<unitIds.Length; j++)
            {
                deck.Add(unitIds[j]);
            }
        }

        Shuffle(deck);

        return deck;
    }

    void AddPlayerForge()
    {
        var unitIdIndex = Random.Range(0, playerDeck.Count);
        var unitId = playerDeck[unitIdIndex];
        playerDeck.RemoveAt(unitIdIndex);

        var forge = new UnitForge(unitId, Random.Range(1, 4));
        PlayerForges.Add(forge);
        UnasignedPlayerForges.Add(forge);
    }

    void AddBotForge()
    {
        var unitIdIndex = Random.Range(0, botDeck.Count);
        var unitId = botDeck[unitIdIndex];
        botDeck.RemoveAt(unitIdIndex);

        var forge = new UnitForge(unitId, Random.Range(1, 4));
        BotForges.Add(forge);
        UnasignedBotForges.Add(forge);
    }

    private void Start()
    {
        timerRunning = true;
        timeForPlaceBlock = TIME_FOR_TURN;
        Players[CurrentPlayerIndex].StartMove();
    }

    private void Update()
    {
        if (timerRunning)
        {
            timeForPlaceBlock -= Time.deltaTime;
            if (timeForPlaceBlock <= 0.0f)
            {
                timeForPlaceBlock = 0.0f;
                timerRunning = false;
                if (CurrentPlayerIndex == 3)
                {
                    float sanityDelay = Human.CancelMove();
                    NextPlayer(sanityDelay);
                }
            }
        }
    }

    public void NextPlayer(float sanityDelay)
    {
        timerRunning = false;
        timeForPlaceBlock = TIME_FOR_TURN;

        CurrentPlayerIndex++;
        CurrentPlayerIndex %= 4;

        FinishedMoves++;

        if (DaysToBattle == 0 && FinishedMoves % 4 == 0)
        {
            MatchOver = true;
            StartCoroutine(FinishMatch(sanityDelay));
            return;
        }

        StartCoroutine(StartMoveAfter1Sec(sanityDelay));
    }

    IEnumerator StartMoveAfter1Sec(float sanityDelay)
    {
        if(FinishedMoves % 4 == 0)
        {
            CurrentDay++;
        }

        yield return new WaitForSeconds(sanityDelay);
        ProduceUnits();
        if(sanityDelay > 0.0f)
        {
            yield return new WaitForSeconds(0.5f);
        }

        timerRunning = true;

        if(CurrentPlayerIndex == 1 || CurrentPlayerIndex == 3)
        {
            yield return StartCoroutine(Board.SpawnNewToken());
        }

        Players[CurrentPlayerIndex].StartMove();
    }

    IEnumerator FinishMatch(float sanityDelay)
    {
        yield return new WaitForSeconds(sanityDelay);

        ProduceUnits();
        yield return new WaitForSeconds(1.0f);

        MatchRefs.Instance.Saying.Show("Battle Time!");

        yield return new WaitForSeconds(3.0f);

        SetBattleParams();

        SceneManager.LoadScene("Battle");
    }

    void SetBattleParams()
    {
        var appMain = AppMain.Instance;

        appMain.BattleParams = new BattleParams(
            playerTroops: humanFighters.ToArray(),
            opponentTroops: botFighters.ToArray()
        );
    }

    private void OnPlayerRecruit(string unitId)
    {
        var fighter = Instantiate(StandbyFighterPrefab);
        fighter.Setup(humanFighters.Count, unitId, true);

        humanFighters.Add(unitId);
    }

    private void OnBotRecruit(string unitId)
    {
        var fighter = Instantiate(StandbyFighterPrefab);
        fighter.Setup(botFighters.Count, unitId, false);

        botFighters.Add(unitId);
    }

    private void FixedUpdate()
    {
        float humanTarget = Formation.TotalHorizontalSpacing(humanFighters.Count);
        HumanFighterSpan = HumanFighterSpan * 0.9f + humanTarget * 0.1f;

        float botTarget = Formation.TotalHorizontalSpacing(botFighters.Count);
        BotFighterSpan = BotFighterSpan * 0.9f + botTarget * 0.1f;
    }

    public void OnPlayerCollectResource(TokenType tokenType)
    {
        var resourceType = ToResource(tokenType);

        foreach (var forge in PlayerForges)
        {
            forge.OnCollect(resourceType);
        }
    }

    public void OnBotCollectResource(TokenType tokenType)
    {
        var resourceType = ToResource(tokenType);

        foreach (var forge in BotForges)
        {
            forge.OnCollect(resourceType);
        }
    }

    private ResourceType ToResource(TokenType tokenType)
    {
        switch(tokenType)
        {
            case TokenType.Gems:
                return ResourceType.Gems;
            case TokenType.Wood:
                return ResourceType.Wood;
            case TokenType.Horses:
                return ResourceType.Horses;
            case TokenType.Iron:
                return ResourceType.Iron;
            case TokenType.Gold:
                return ResourceType.Gold;
        }

        Debug.LogError("Not handled");

        return ResourceType.Gold;
    }

    void ProduceUnits()
    {
        int count = 0;
        foreach(var forge in PlayerForges)
        {
            for(int i=0; i<forge.ProducedUnitCount(); i++)
            {
                OnPlayerRecruit(forge.UnitConfig.id);
                count++;
            }

            if(forge.CanProduceUnit())
            {
                playerDeck.Add(forge.UnitConfig.id);
            }
        }

        foreach (var forge in BotForges)
        {
            for (int i = 0; i < forge.ProducedUnitCount(); i++)
            {
                OnBotRecruit(forge.UnitConfig.id);
            }

            if (forge.CanProduceUnit())
            {
                botDeck.Add(forge.UnitConfig.id);
            }
        }

        if(count == 0)
        {

        } else if(count < 2)
        {
            if(Random.Range(0, 2) == 0)
            {
                MatchRefs.Instance.Saying.Show("Nice!");
            } else
            {
                MatchRefs.Instance.Saying.Show("Good!");
            }
            
        } else if(count < 6)
        {
            MatchRefs.Instance.Saying.Show("Great!");
        } else if(count < 10)
        {
            MatchRefs.Instance.Saying.Show("Super!");
        } else
        {
            MatchRefs.Instance.Saying.Show("Perfect!");
        }

        PlayerForges.RemoveAll((forge) => forge.CanProduceUnit());
        BotForges.RemoveAll((forge) => forge.CanProduceUnit());

        while (PlayerForges.Count < 4)
        {
            AddPlayerForge();
        }

        while (BotForges.Count < 4)
        {
            AddBotForge();
        }

        foreach(var forgePanel in ForgePanels)
        {
            forgePanel.ReloadIfNeeded();
        }

        foreach (var forgePanel in BotForgePanels)
        {
            forgePanel.ReloadIfNeeded();
        }
    }

    public static void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;

            int k = Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }

}
