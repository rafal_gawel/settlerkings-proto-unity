﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MatchMakingMain : MonoBehaviour
{

    public GameObject RulesParent;
    public SearchOpponentPanel Opponent;

    private void Awake()
    {
        StartCoroutine(FindOpponents());
    }

    IEnumerator FindOpponents()
    {
        List<int> ids = new List<int>();

        for(int i=0; i<50; i++)
        {
            ids.Add(i);
        }

        for (int i = 0; i < ids.Count; i++)
        {
            var temp = ids[i];
            int randomIndex = Random.Range(i, ids.Count);
            ids[i] = ids[randomIndex];
            ids[randomIndex] = temp;
        }

        yield return new WaitForSeconds(Random.Range(2.0f, 3.0f));

        Opponent.Setup(ids[1]);

        yield return new WaitForSeconds(5.0f);

        AppMain.Instance.OpponentParams = new OpponentParams
        {
            Ids = new int[] {ids[0], ids[1], ids[2]}
        };

        SceneManager.LoadScene("Match");
    }

}
