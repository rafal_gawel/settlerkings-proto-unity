﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelsMain : MonoBehaviour
{
    public TextMeshProUGUI CoinsText;

    public ModePanel EasyPanel;
    public ModePanel HardPanel;
    public ModePanel ProPanel;

    void Awake()
    {
        var save = SaveStorage.Load();

        if(save.Coins < 100)
        {
            save.Coins = 100;
            SaveStorage.Save(save);
        }

        CoinsText.text = save.Coins.ToString();

        EasyPanel.Setup(save.Coins, 100);
        HardPanel.Setup(save.Coins, 400);
        ProPanel.Setup(save.Coins, 1000);
    }

    public void OnClickEasy()
    {
        TakeFee(100);

        var save = SaveStorage.Load();

        if (!save.SeenTutorial)
        {
            save.SeenTutorial = true;
            SaveStorage.Save(save);
            LoadMap(0, 100);
        }
        else
        {
            LoadMap(1 + Random.Range(0, 3), 100);
        }
    }

    public void OnClickHard()
    {
        TakeFee(400);
        LoadMap(4 + Random.Range(0, 3), 400);
    }

    public void OnClickPro()
    {
        TakeFee(1000);
        LoadMap(7 + Random.Range(0, 3), 1000);
    }

    void TakeFee(int cost)
    {
        var save = SaveStorage.Load();
        save.Coins -= cost;
        SaveStorage.Save(save);
    }

    void LoadMap(int mapId, int entryFee)
    {
        var save = SaveStorage.Load();

        AppMain.Instance.MatchParams = new MatchParams
        {
            EntryFee = entryFee,
            MapId = mapId,
            BoardRows = 10,
            BoardCols = 20,
            CellSize = 24,
            PlayerUnitsIds = UnitUnlocked.Unlocked(save.Wins),
            OpponentUnitsIds = UnitUnlocked.Unlocked(save.Wins + Random.Range(-1, 2)),
        };

        SceneManager.LoadScene("MatchMaking");
    }
}
