﻿using TMPro;
using UnityEngine;

public class Saying : MonoBehaviour
{
    public TextMeshProUGUI Text;

    private float AnimationProgress = 1.0f;

    private void Start()
    {
        Text.color = new Color(1.0f, 1.0f, 1.0f, 0.0f);
    }

    public void Show(string text)
    {
        Text.text = text;
        AnimationProgress = 0.0f;
    }

    private void Update()
    {
        AnimationProgress += Time.deltaTime * 0.5f;

        if(AnimationProgress > 1.0f)
        {
            AnimationProgress = 1.0f;
        }

        if(AnimationProgress < 0.3f)
        {
            float t = AnimationProgress / 0.3f;

            Text.color = Color.white;
            
            float scale = 0.1f + 0.9f * Easing.Back.Out(t);
            Text.transform.localScale = new Vector3(scale, scale, 1.0f);
        } else
        {
            float t = (AnimationProgress - 0.3f) / 0.7f;

            Text.color = new Color(1.0f, 1.0f, 1.0f, 1.0f - Easing.Sinusoidal.In(t));
            Text.transform.localScale = Vector3.one;
        }
    }
}
