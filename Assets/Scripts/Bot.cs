﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

struct Move
{
    public int Row;
    public int Col;
    public int Rotation;
    public int Score;
}

enum BotBehaviour
{
    Throw,
    Cheat,
}

public class Bot : Player
{
    public int PlayerId;

    public Stencil Stencil;
    public Deck Deck;
    public Moves Moves;

    int FinishedMoves = 0;

    private BotBehaviour behaviour = BotBehaviour.Cheat;


    private void Start()
    {
        var save = SaveStorage.Load();
        behaviour = BotThrowConfig.BotShouldThrow(save.PlayedGames) ?
            BotBehaviour.Throw :
            BotBehaviour.Cheat;

        Stencil.SetShape(Deck.Cards[0].Shape);
    }

    public override void StartMove()
    {
        if (PlayerId == 0 || PlayerId == 2)
        {
            MatchMain.Instance.NextPlayer(0.0f);
        }
        else
        {
            StartCoroutine(MoveCooroutine());
        }
    }

    public override Stencil GetStencil()
    {
        return Stencil;
    }

    IEnumerator MoveCooroutine()
    {
        if(FinishedMoves >= Deck.Cards.Count)
        {
            yield break;
        }

        Stencil.CenterRowCol();
        Stencil.gameObject.SetActive(true);

        var card = Deck.Cards[FinishedMoves];

        Stencil.SetShape(card.Shape);

        yield return new WaitForSeconds(Random.Range(1.0f, 6.0f));

        var cfg = AppMain.Instance.MatchParams;

        PointsCalc pointsCalc = new PointsCalc();
        List<Move> moves = new List<Move>();
        bool movesEmpty = true;

        ResourceScoring scoring = new ResourceScoring();

        foreach(var forge in MatchMain.Instance.BotForges)
        {
            scoring.Gems = Mathf.Max(scoring.Gems, forge.NeededResource(ResourceType.Gems));
            scoring.Wood = Mathf.Max(scoring.Gems, forge.NeededResource(ResourceType.Wood));
            scoring.Gold = Mathf.Max(scoring.Gems, forge.NeededResource(ResourceType.Gold));
            scoring.Horses = Mathf.Max(scoring.Gems, forge.NeededResource(ResourceType.Horses));
            scoring.Iron = Mathf.Max(scoring.Gems, forge.NeededResource(ResourceType.Iron));
        }

        for (int rotation = 0; rotation < 4; rotation++)
        {
            for (int row = -5; row < cfg.BoardRows; row++)
            {
                for (int col = -5; col < cfg.BoardCols; col++)
                {
                    Stencil.row = row;
                    Stencil.col = col;

                    if (Stencil.CanStamp())
                    {
                        MatchMain.Instance.PointsCalc.Clone(pointsCalc);

                        Stencil.DryStamp(pointsCalc);

                        var score = pointsCalc.GetKingdomPoints(PlayerId, scoring);

                        if(score > 0 || movesEmpty)
                        {
                            movesEmpty = false;
                            moves.Add(
                                new Move
                                {
                                    Row = row,
                                    Col = col,
                                    Rotation = rotation,
                                    Score = score,
                                }
                            );
                        }


                    }
                }
            }

            Stencil.RotateShape();
        }


        Shuffle(moves);
        moves.Sort((a, b) => b.Score.CompareTo(a.Score));

        Stencil.SetShape(card.Shape);
        Stencil.CenterRowCol();

        Move selectedMove;

        int maxIndex;

        if(behaviour == BotBehaviour.Cheat)
        {
            maxIndex = Mathf.FloorToInt(Random.value * moves.Count * 0.01f);
        } else
        {
            maxIndex = Mathf.FloorToInt(Random.value * moves.Count * 0.05f);
        }

        if(maxIndex == 0)
        {
            maxIndex = 1;
        }

        selectedMove = moves[Random.Range(0, maxIndex)];

        int bestRotation = selectedMove.Rotation;
        int bestRow = selectedMove.Row;
        int bestCol = selectedMove.Col;
        int bestScore = selectedMove.Score;

        for (int rotation = 0; rotation < bestRotation; rotation++)
        {
            Stencil.RotateShape();
            yield return new WaitForSeconds(Random.Range(0.05f, 0.2f));
        }

        yield return new WaitForSeconds(Random.Range(0.3f, 0.5f));

        Debug.Log("BEST: " + bestScore);


        while(Stencil.row != bestRow || Stencil.col != bestCol)
        {
            int rowAbs = Mathf.Abs(Stencil.row - bestRow);
            int total = rowAbs + Mathf.Abs(Stencil.col - bestCol);

            if(Random.Range(0, total) < rowAbs)
            {
                if(Stencil.row > bestRow)
                {
                    Stencil.row--;
                } else
                {
                    Stencil.row++;
                }
            } else
            {
                if (Stencil.col > bestCol)
                {
                    Stencil.col--;
                }
                else
                {
                    Stencil.col++;
                }
            }

            Stencil.SyncPosition();

            yield return new WaitForSeconds(Random.Range(0.1f, 0.2f));
        }

        Stencil.row = bestRow;
        Stencil.col = bestCol;
        Stencil.SyncPosition();

        yield return new WaitForSeconds(Random.Range(0.2f, 1.0f));

        float sanityDelay = Stencil.Stamp();

        FinishedMoves++;
        Moves.SetFinishedMoves(FinishedMoves);

        Stencil.gameObject.SetActive(false);

        MatchMain.Instance.NextPlayer(sanityDelay);
    }

    public static void Shuffle<T>(IList<T> list)
    {
        int n = list.Count;
        while (n > 1)
        {
            n--;

            int k = Random.Range(0, n + 1);
            T value = list[k];
            list[k] = list[n];
            list[n] = value;
        }
    }
}
