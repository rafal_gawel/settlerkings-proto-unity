﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

enum SummaryAnimPhase
{
    NotStarted,
    Decreasing,
    Pause,
    Increasing,
}

public class SummaryRow : MonoBehaviour
{
    public int SummaryId;

    public TextMeshProUGUI NickText;
    public TextMeshProUGUI PointsText;
    public TextMeshProUGUI CoinsText;
    public Image Portrait;
    public Image Background;

    public Sprite[] Backgrounds;

    public Avatars Avatars;
    public Sprite You;

    public int GetCurrentCoins()
    {
        return Mathf.RoundToInt(CurrentCoins);
    }

    public int CoinsInBank()
    {
        var cfg = AppMain.Instance.SummaryParams.Players[SummaryId];

        switch (animPhase)
        {
            case SummaryAnimPhase.NotStarted:
                return 0;
            case SummaryAnimPhase.Decreasing:
                return Mathf.RoundToInt(Mathf.Lerp(0, cfg.Reward, animationProgress));
            case SummaryAnimPhase.Pause:
                return cfg.Reward;
            case SummaryAnimPhase.Increasing:
                return Mathf.RoundToInt(Mathf.Lerp(cfg.Reward, 0, animationProgress));
        }

        return 0;
    }

    float CurrentCoins;

    SummaryAnimPhase animPhase = SummaryAnimPhase.NotStarted;
    float animationProgress = 0.0f;

    void Start()
    {
        var cfg = AppMain.Instance.SummaryParams.Players[SummaryId];

        if(cfg.PlayerId == 3)
        {
            NickText.text = "YOU";
            Portrait.sprite = You;
        } else
        {
            NickText.text = Names.Values[cfg.OpponentId];
            Portrait.sprite = Avatars.Sprites[cfg.OpponentId];
        }

        CurrentCoins = AppMain.Instance.SummaryParams.EntryFee;

        PointsText.text = cfg.Points.ToString();
        Background.sprite = Backgrounds[cfg.PlayerId];
        CoinsText.text = Mathf.RoundToInt(CurrentCoins).ToString();
    }

    private void FixedUpdate()
    {
        switch(animPhase)
        {
            case SummaryAnimPhase.NotStarted:
                animationProgress += Time.deltaTime / 3.0f;
                break;
            case SummaryAnimPhase.Decreasing:
                animationProgress += Time.deltaTime / 1.0f;
                break;
            case SummaryAnimPhase.Pause:
                animationProgress += Time.deltaTime / 1.0f;
                break;
            case SummaryAnimPhase.Increasing:
                animationProgress += Time.deltaTime / 1.0f;
                break;
        }

        if(animationProgress >= 1.0f)
        {
            switch (animPhase)
            {
                case SummaryAnimPhase.NotStarted:
                    animPhase = SummaryAnimPhase.Decreasing;
                    animationProgress = 0.0f;
                    break;
                case SummaryAnimPhase.Decreasing:
                    animPhase = SummaryAnimPhase.Pause;
                    animationProgress = 0.0f;
                    break;
                case SummaryAnimPhase.Pause:
                    animPhase = SummaryAnimPhase.Increasing;
                    animationProgress = 0.0f;
                    break;
                case SummaryAnimPhase.Increasing:
                    animationProgress = 1.0f;
                    break;
            }
        }

        var cfg = AppMain.Instance.SummaryParams.Players[SummaryId];

        switch (animPhase)
        {
            case SummaryAnimPhase.NotStarted:
                CurrentCoins = AppMain.Instance.SummaryParams.EntryFee;
                break;
            case SummaryAnimPhase.Decreasing:
                CurrentCoins = Mathf.Lerp(AppMain.Instance.SummaryParams.EntryFee, 0.0f, animationProgress);
                break;
            case SummaryAnimPhase.Pause:
                CurrentCoins = 0.0f;
                break;
            case SummaryAnimPhase.Increasing:
                CurrentCoins = Mathf.Lerp(0.0f, cfg.Reward, animationProgress);
                break;
        }

        CoinsText.text = Mathf.RoundToInt(CurrentCoins).ToString();
    }
}
