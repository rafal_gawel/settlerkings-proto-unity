﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Stencil : MonoBehaviour
{
    public int PlayerIndex;
    public Board Board;
    public Avatar Avatar;
    public Color TintColor;

    public Sprite Rounded;
    public Sprite Cross;

    public SpriteRenderer[] Blocks;
    public SpriteRenderer[] Fills;
    public SpriteRenderer[] Borders;

    int startDragRow = 0;
    int startDragCol = 0;

    public int row = 5;
    public int col = 10;

    string[] shape = new string[]
    {
        "     ",
        " ##  ",
        " #   ",
        " ##  ",
        "     ",
    };

    public void CenterRowCol()
    {
        row = AppMain.Instance.MatchParams.BoardRows / 2 - 2;
        col = AppMain.Instance.MatchParams.BoardCols / 2 - 2;

        SyncPosition();
    }

    private void Start()
    {
        ScaleBlocks();
        SyncPosition();
        SyncShape();

        gameObject.SetActive(false);

        

        for(int i=0; i<Fills.Length; i++)
        {
            Fills[i].color = Color.Lerp(Color.white, TintColor, 0.3f);
        }

        for(int i=0; i<Blocks.Length; i++)
        {
            Blocks[i].color = TintColor;
        }
        for (int i = 0; i < Borders.Length; i++)
        {
            Borders[i].color = Color.Lerp(Color.white, TintColor, 0.3f); ;
        }
    }

    void ScaleBlocks()
    {
        var cfg = AppMain.Instance.MatchParams;

        for(int i=0; i<5; i++)
        {
            for(int j=0; j<5; j++)
            {
                Blocks[i + j * 5].transform.localPosition = new Vector2(
                    (i - 2) * cfg.CellSize,
                    (2 - j) * cfg.CellSize);
            }
        }

        for(int i=0; i<Blocks.Length; i++)
        {
            Blocks[i].transform.localScale = new Vector3(cfg.CellSize, cfg.CellSize, 1.0f);
        }
    }

    public void OnStartDrag()
    {
        startDragRow = row;
        startDragCol = col;
    }

    public void OnDrag(Vector2 dragOffset)
    {
        row = startDragRow - ToBoardOffset(dragOffset.y);
        col = startDragCol + ToBoardOffset(dragOffset.x);

        ClampBoardPosition();

        SyncPosition();
    }

    int ToBoardOffset(float n)
    {
        return Mathf.RoundToInt(n / AppMain.Instance.MatchParams.CellSize);
    }

    public void SyncPosition()
    {
        var cfg = AppMain.Instance.MatchParams;
        transform.localPosition = new Vector2(
            -cfg.BoardCols * cfg.CellSize / 2 + 5 * cfg.CellSize / 2 + col * cfg.CellSize,
            cfg.BoardRows * cfg.CellSize / 2 - 5 * cfg.CellSize / 2 - row * cfg.CellSize);
        SyncColor();
    }

    void SyncShape()
    {
        for(int row=0; row<shape.Length; row++)
        {
            for(int col=0; col<shape[row].Length; col++)
            {
                Blocks[row * 5 + col].gameObject.SetActive(shape[row][col] == '#');
            }
        }
    }

    void SyncColor()
    {
        for (int row = 0; row < shape.Length; row++)
        {
            for (int col = 0; col < shape[row].Length; col++)
            {
                if (shape[row][col] == '#')
                {
                    int r = row + this.row;
                    int c = col + this.col;

                    var token = Board.GetToken(r, c);
                    if (token == null || token.Occupied)
                    {
                        Blocks[row * 5 + col].sprite = Cross;
                    } else
                    {
                        Blocks[row * 5 + col].sprite = Rounded;
                    }
                }
            }
        }
    }

    void ClampBoardPosition()
    {
        var cfg = AppMain.Instance.MatchParams;

        int marginTop = 0;

        for(int i=0; i<shape.Length; i++)
        {
            if(!shape[i].Contains("#"))
            {
                marginTop++;
            } else
            {
                break;
            }
        }

        int marginBottom = 0;
        for (int i = shape.Length-1; i >= 0; i--)
        {
            if (!shape[i].Contains("#"))
            {
                marginBottom++;
            }
            else
            {
                break;
            }
        }

        row = Mathf.Clamp(row, -marginTop, cfg.BoardRows - 5 + marginBottom);

        int marginLeft = 0;

        for(int i=0; i<5; i++)
        {
            if(!CheckShapeCol(i))
            {
                marginLeft++;
            } else
            {
                break;
            }
        }

        int marginRight = 0;

        for (int i = 4; i >= 0; i--)
        {
            if (!CheckShapeCol(i))
            {
                marginRight++;
            }
            else
            {
                break;
            }
        }

        col = Mathf.Clamp(col, -marginLeft, cfg.BoardCols - 5 + marginRight);
    }

    bool CheckShapeCol(int col)
    {
        for(int i=0; i<shape.Length; i++)
        {
            if(shape[i][col] == '#')
            {
                return true;
            }
        }

        return false;
    }

    public void RotateShape()
    {
        var newShape = new StringBuilder[]
        {
            new StringBuilder("     "),
            new StringBuilder("     "),
            new StringBuilder("     "),
            new StringBuilder("     "),
            new StringBuilder("     "),
        };

        for (int row = 0; row < 5; row++)
        {
            for (int col = 0; col < 5; col++)
            {
                newShape[row][4 - col] = shape[col][row];
            }
        }

        shape = new string[5];

        for (int row = 0; row < 5; row++)
        {
            shape[row] = newShape[row].ToString();
        }

        SyncShape();
        ClampBoardPosition();
        SyncPosition();
    }

    public void SetShape(string[] newShape)
    {
        shape = newShape;

        SyncShape();
        ClampBoardPosition();
        SyncPosition();
    }

    public bool CanStamp()
    {
        for (int row = 0; row < shape.Length; row++)
        {
            for (int col = 0; col < shape[row].Length; col++)
            {
                if (shape[row][col] == '#')
                {
                    int r = row + this.row;
                    int c = col + this.col;

                    var token = Board.GetToken(r, c);
                    if (token == null || token.Occupied)
                    {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public float Stamp()
    {
        if(!CanStamp())
        {
            return -1.0f;
        }

        for (int row = 0; row < shape.Length; row++)
        {
            for (int col = 0; col < shape[row].Length; col++)
            {
                if (shape[row][col] == '#')
                {
                    int r = row + this.row;
                    int c = col + this.col;

                    var token = Board.GetToken(r, c);

                    if (!token.Occupied) // ?
                    {
                        MatchMain.Instance.PointsCalc.AddKingdomField(PlayerIndex, r, c);
                    }
                }
            }
        }

        var matchMain = MatchMain.Instance;

        matchMain.PointsCalc.FillShapes(PlayerIndex);

        float delay = 0.0f;

        Board.SyncSelections((token) =>
        {
            var tokenType = token.TokenType;

            SpawnFlyingResource(token, () => {
                if(PlayerIndex == 1)
                {
                    matchMain.OnBotCollectResource(tokenType);
                } else
                {
                    matchMain.OnPlayerCollectResource(tokenType);
                }

                matchMain.PointsCalc.AddToKingdom(PlayerIndex, tokenType);
            }, delay);

            delay += 0.2f;
        });

        SyncColor();

        return delay + 1.0f;
    }

    void SpawnFlyingResource(Token token, System.Action action, float delay)
    {
        if(token.TokenType == TokenType.None)
        {
            return;
        }     

        var refs = MatchRefs.Instance;

        var flyingResource = Instantiate(refs.FlyingResourcePrefab, refs.FlyingResourcesParent.transform);
        flyingResource.animationDelay = delay;
        flyingResource.transform.position = token.transform.position;
        flyingResource.Sprite.sprite = token.Image.sprite;
        flyingResource.OnFinished = action;
    }

    public void DryStamp(PointsCalc pointsCalc)
    {
        for (int row = 0; row < shape.Length; row++)
        {
            for (int col = 0; col < shape[row].Length; col++)
            {
                if (shape[row][col] == '#')
                {
                    int r = row + this.row;
                    int c = col + this.col;

                    var token = Board.GetToken(r, c);

                    if (!token.Occupied)
                    {
                        pointsCalc.AddKingdomField(PlayerIndex, r, c);
                    }
                }
            }
        }

        pointsCalc.FillShapes(PlayerIndex);

        var fields = pointsCalc.kingdoms[PlayerIndex].Fields;

        for (int row = 0; row < fields.GetLength(0); row++)
        {
            for (int col = 0; col < fields.GetLength(1); col++)
            {
                if (!fields[row, col]) continue;

                var token = Board.GetToken(row, col);

                if (!token.Occupied)
                {
                    pointsCalc.AddToKingdom(PlayerIndex, token.TokenType);
                }
            }
        }
    }

    public void SetProgress(float progress)
    {
        for(int i=0; i<Fills.Length; i++)
        {
            Fills[i].material.SetFloat("_Arc2", (1.0f - progress) * 360.0f);
        }
    }
}
