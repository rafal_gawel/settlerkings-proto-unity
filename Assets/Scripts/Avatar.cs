﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Avatar : MonoBehaviour
{
    public Avatars Avatars;
    public TextMeshProUGUI Nick;
    public Image Portrait;
    public Image Highlight;
    public int PlayerIndex;

    private void Awake()
    {
        if (PlayerIndex < 3)
        {
            int id = AppMain.Instance.OpponentParams.Ids[PlayerIndex];

            Nick.text = Names.Values[id];
            Portrait.sprite = Avatars.Sprites[id];
        }
    }

    private void Update()
    {
        UpdateScale();

        var matchMain = MatchMain.Instance;


        var stencil = MatchMain.Instance.Players[PlayerIndex].GetStencil();

        if(matchMain.CurrentPlayerIndex == PlayerIndex)
        {
            stencil.SetProgress(1.0f - matchMain.TimeForTurnNormalized);
        } else
        {
            stencil.SetProgress(0.0f);
        }
    }

    float scaleAnimation = 1.0f;

    void UpdateScale()
    {
        scaleAnimation += Time.deltaTime * 1.5f;

        if(scaleAnimation > 1.0f)
        {
            var main = MatchMain.Instance;
            var continueAnimaiton = !main.MatchOver && main.CurrentPlayerIndex == PlayerIndex && (
                PlayerIndex == 1 || PlayerIndex == 3);

            if(continueAnimaiton)
            {
                scaleAnimation = 0.0f;
            } else
            {
                scaleAnimation = 1.0f;
            }
        }

        var scale = 1.0f + Mathf.Sin(scaleAnimation * Mathf.PI) * 0.1f;

        Portrait.transform.localScale = new Vector3(scale, scale, 1.0f);
    }

}
