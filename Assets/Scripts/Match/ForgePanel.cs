﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ForgePanel : MonoBehaviour
{
    public bool isBot;

    public Image[] PortraitImages;
    public Transform ResourcesParent;
    public PartIcon PartIconPrefab;
    public RectTransform Content;

    public UnitPortraits Portraits;
    public UnitPortraits PortraitsGray;

    private UnitForge forge;

    private bool lastTimeCanProduce = false;
    private float scaleAnimation = 1.0f;
    private float reloadingAnimation = 0.0f;
    private bool isReloading = false;

    void Start()
    {
        Reload();
    }

    void Reload()
    {
        var matchMain = MatchMain.Instance;

        if(isBot)
        {
            forge = matchMain.UnasignedBotForges[0];
            matchMain.UnasignedBotForges.RemoveAt(0);
        } else
        {
            forge = matchMain.UnasignedPlayerForges[0];
            matchMain.UnasignedPlayerForges.RemoveAt(0);
        }

        RecreateParts();

        for(int i=0; i<PortraitImages.Length; i++)
        {
            var portrait = PortraitImages[i];
            portrait.sprite = Portraits.GetForId(forge.UnitConfig.id);
            portrait.gameObject.SetActive(i < forge.Multiplier);
        }
    }

    private void RecreateParts()
    {
        foreach (Transform child in ResourcesParent)
        {
            Destroy(child.gameObject);
        }

        RecreateParts(ResourceType.Wood);
        RecreateParts(ResourceType.Iron);
        RecreateParts(ResourceType.Horses);
        RecreateParts(ResourceType.Gems);
        RecreateParts(ResourceType.Gold);
    }

    private void RecreateParts(ResourceType resourceType)
    {
        for(int i=0; i<forge.UnitConfig.GetCost(resourceType); i++)
        {
            var partIcon = Instantiate(PartIconPrefab, ResourcesParent);

            partIcon.Setup(i + 1, resourceType, forge);
        }
    }

    private void Update()
    {
        bool canProduce = forge.CanProduceUnit();

        if (!lastTimeCanProduce && canProduce)
        {
            scaleAnimation = 0.0f;
            lastTimeCanProduce = canProduce;
        }

        if (canProduce)
        {
            scaleAnimation += Time.deltaTime * 3.0f;
        }

        if (scaleAnimation > 1.0f)
        {
            scaleAnimation = 1.0f;
        }


        float scale = 0.7f + 0.3f * Easing.Back.Out(scaleAnimation);
        scale *= forge.UnitConfig.scale;
        foreach(var portrait in PortraitImages)
        {
            portrait.transform.localScale = new Vector3(isBot ? -scale : scale, scale, scale);
        }

        if(isReloading)
        {
            reloadingAnimation += Time.deltaTime * 2.0f;

            if(reloadingAnimation >= 1.0f)
            {
                reloadingAnimation = 1.0f;
                isReloading = false;

                Reload();
            }
        } else
        {
            reloadingAnimation -= Time.deltaTime * 2.0f;
            if(reloadingAnimation < 0.0f)
            {
                reloadingAnimation = 0.0f;
            }
        }

        float displY = Easing.Back.In(reloadingAnimation) * 100.0f;

        Content.anchoredPosition = new Vector2(0.0f, displY);
    }

    public void ReloadIfNeeded()
    {
        if(forge.CanProduceUnit())
        {
            isReloading = true;
        }
    }

}
