﻿using UnityEngine;

public class UnitForge
{
    public readonly UnitConfig UnitConfig;

    public int Gems { get; private set; } = 0;
    public int Wood { get; private set; } = 0;
    public int Horses { get; private set; } = 0;
    public int Iron { get; private set; } = 0;
    public int Gold { get; private set; } = 0;

    public int Multiplier { get; private set; } = 0;

    public UnitForge(string unitId, int multiplier)
    {
        UnitConfig = UnitConfig.GetForId(unitId);
        Multiplier = multiplier;
    }

    public void OnCollect(ResourceType resourceType)
    {
        switch(resourceType)
        {
            case ResourceType.Gems:
                Gems++;
                break;
            case ResourceType.Wood:
                Wood++;
                break;
            case ResourceType.Horses:
                Horses++;
                break;
            case ResourceType.Iron:
                Iron++;
                break;
            case ResourceType.Gold:
                Gold++;
                break;
        }
    }

    public bool CanProduceUnit()
    {
        return UnitConfig.gems <= Gems &&
             UnitConfig.wood <= Wood &&
             UnitConfig.horses <= Horses &&
             UnitConfig.iron <= Iron &&
             UnitConfig.gold <= Gold;
    }

    public int ProducedUnitCount()
    {
        return Mathf.Min(
                FullUnits(UnitConfig.gems, Gems),
                FullUnits(UnitConfig.wood, Wood),
                FullUnits(UnitConfig.horses, Horses),
                FullUnits(UnitConfig.iron, Iron),
                FullUnits(UnitConfig.gold, Gold)) * Multiplier;
    }

    int FullUnits(int cost, int parts)
    {
        if(cost == 0)
        {
            return 999;
        }

        return parts / cost;
    }

    public int GetAmount(ResourceType resourceType)
    {
        switch (resourceType)
        {
            case ResourceType.Gems:
                return Gems;
            case ResourceType.Wood:
                return Wood;
            case ResourceType.Horses:
                return Horses;
            case ResourceType.Iron:
                return Iron;
            case ResourceType.Gold:
                return Gold;
        }

        Debug.LogError("Type not handled " + resourceType);

        return 0;
    }

    public int NeededResource(ResourceType resourceType)
    {
        return Mathf.Max(0, UnitConfig.GetCost(resourceType) - GetAmount(resourceType));
    }
}