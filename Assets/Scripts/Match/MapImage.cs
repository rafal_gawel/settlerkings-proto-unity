﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapImage : MonoBehaviour
{

    public Sprite[] Sprites;

    void Start()
    {
        SpriteRenderer sr = GetComponent<SpriteRenderer>();

        sr.sprite = Sprites[AppMain.Instance.MatchParams.MapId];
    }
}
