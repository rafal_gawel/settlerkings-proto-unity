﻿using TMPro;
using UnityEngine;

public class ContinueButton : MonoBehaviour
{
    public TextMeshProUGUI Counter;

    float currentTime = 0.0f;

    public void StartCounting(float time)
    {
        currentTime = time;
    }

    private void Update()
    {
        currentTime -= Time.deltaTime;

        if(currentTime < 0)
        {
            currentTime = 0.0f;
        }

        Counter.text = Mathf.FloorToInt(currentTime).ToString();
    }

}
