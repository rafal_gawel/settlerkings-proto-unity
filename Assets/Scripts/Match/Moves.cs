﻿using TMPro;
using UnityEngine;

public class Moves : MonoBehaviour
{
    public TextMeshProUGUI Text;

    public void SetFinishedMoves(int moves)
    {
        Text.text = "MOVES: " + (8 - moves);
    }
}
