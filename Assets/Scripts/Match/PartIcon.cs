﻿using UnityEngine;
using UnityEngine.UI;

public class PartIcon : MonoBehaviour
{
    public Image Icon;

    public ResourceIcons ResourceIcons;
    public ResourceIcons ResourceIconsGray;

    private int minAmount;
    private ResourceType resourceType;
    private UnitForge forge;

    private bool lastTimeCollected = false;
    private float collectionAnimation = 1.0f;

    public void Setup(int minAmount, ResourceType resourceType, UnitForge forge)
    {
        this.minAmount = minAmount;
        this.resourceType = resourceType;
        this.forge = forge;

        Update();
    }

    private void Update()
    {
        bool collected = forge.GetAmount(resourceType) >= minAmount;

        if(!lastTimeCollected && collected)
        {
            collectionAnimation = 0.0f;
            lastTimeCollected = collected;
        }

        if(collected)
        {
            collectionAnimation += Time.deltaTime * 3.0f;
        }

        if(collectionAnimation > 1.0f)
        {
            collectionAnimation = 1.0f;
        }

        if (collected)
        {
            Icon.sprite = ResourceIcons.GetByType(resourceType);
        } else
        {
            Icon.sprite = ResourceIconsGray.GetByType(resourceType);
        }

        float scale = 0.7f + 0.3f * Easing.Back.Out(collectionAnimation);
        transform.localScale = new Vector3(scale, scale, scale);
    }
}
