﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StandbyFighter : MonoBehaviour
{

    public Transform ScaleTransform;
    public SpriteRenderer Sprite;
    public UnitPortraits Portraits;

    private int index;
    private float showAnimation;
    private bool humanFighter;

    public void Setup(int index, string unitId, bool humanFighter)
    {
        Sprite.sprite = Portraits.GetForId(unitId);
        Sprite.flipX = !humanFighter;
        this.index = index;
        this.humanFighter = humanFighter;
        showAnimation = Random.Range(-0.2f, 0.0f);

        var cfg = UnitConfig.GetForId(unitId);

        var scale = cfg.scale;
        ScaleTransform.localScale = new Vector3(scale, scale, scale);

        Update();
    }

    private void Update()
    {
        Sprite.sortingOrder = Mathf.RoundToInt(-transform.position.y * 100 + transform.position.x);

        showAnimation += Time.deltaTime * 2.0f;

        if(showAnimation > 1.0f)
        {
            showAnimation = 1.0f;
        }

        var matchMain = MatchMain.Instance;
        float span = humanFighter ? matchMain.HumanFighterSpan : matchMain.BotFighterSpan;

        Vector2 position = Formation.GetDisplacement(index, humanFighter) + Formation.GetCenter(humanFighter);
        if(humanFighter)
        {
            position += new Vector2(span / 2.0f, 0.0f);
        } else
        {
            position -= new Vector2(span / 2.0f, 0.0f);
        }

        float startX = humanFighter ? -200.0f : 200.0f;
        position += new Vector2(startX * (1.0f - Easing.Sinusoidal.Out(showAnimation)), 0.0f);

        transform.position = position;
    }
}
