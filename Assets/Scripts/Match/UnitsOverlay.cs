﻿using UnityEngine;

public class UnitsOverlay : MonoBehaviour
{
    public Transform PlayerGrid;

    public UnitCell UnitCellPrefab;


    private void Start()
    {
        var save = SaveStorage.Load();
        var units = UnitUnlocked.Unlocked(save.Wins);

        for (int i = 0; i < units.Length; i++)
        {
            var cell = Instantiate(UnitCellPrefab, PlayerGrid);
            cell.Setup(units[i]);
        }
    }

    public void Open()
    {
        gameObject.SetActive(true);
    }

    public void Close()
    {
        gameObject.SetActive(false);
    }

}
