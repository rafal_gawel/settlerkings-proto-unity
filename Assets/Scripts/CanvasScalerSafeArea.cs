﻿using UnityEngine;
using UnityEngine.UI;

public class CanvasScalerSafeArea : MonoBehaviour
{
    CanvasScaler Scaler;
    Rect LastSafeArea = new Rect(0, 0, 0, 0);

    void Awake()
    {
        Scaler = GetComponent<CanvasScaler>();
        Refresh();
    }

    void Update()
    {
        Refresh();
    }

    void Refresh()
    {
        Rect safeArea = GetSafeArea();

        if (safeArea != LastSafeArea)
            ApplySafeArea(safeArea);
    }

    Rect GetSafeArea()
    {
        return Screen.safeArea;
    }

    void ApplySafeArea(Rect r)
    {
        LastSafeArea = r;

        Scaler.referenceResolution = new Vector2(640.0f * Screen.width / r.width, 360.0f * Screen.height / r.height);
    }
}
