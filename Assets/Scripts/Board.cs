﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public TextAsset[] Maps;

    public SpriteRenderer Background;
    public Stencil Stencil;
    public ResourceIcons ResourceIcons;

    bool isDragging = false;
    Vector2 dragStartPosition = Vector2.zero;

    public Sprite City;
    public Sprite Coal;
    public Sprite Fish;
    public Sprite Gems;
    public Sprite Grain;
    public Sprite Horses;
    public Sprite Ice;
    public Sprite Iron;
    public Sprite Stones;
    public Sprite Volcano;
    public Sprite Water;
    public Sprite Wood;
    public Sprite Gold;

    public Token TokenPrefab;

    private Token[,] tokens = new Token[AppMain.Instance.MatchParams.BoardRows,
        AppMain.Instance.MatchParams.BoardCols];
    private float touchTime = 0.0f;
    private ReignMatrix reignMatrix = new ReignMatrix();

    private List<PossibleSlot> possibleSlots = new List<PossibleSlot>();

    private void Start()
    {
        var map = Maps[AppMain.Instance.MatchParams.MapId];

        var lines = map.text.Split('\n');

        for(int row=0; row<lines.Length; row++)
        {
            var line = lines[row].Split('\t');
            for(int col=0; col<line.Length; col++)
            {
                switch(line[col].ToLower())
                {
                    case "?":
                        possibleSlots.Add(new PossibleSlot
                        {
                            row = row,
                            col = col,
                        });
                        Spawn(row, col, TokenType.None);
                        break;
                    case "city":
                        Spawn(row, col, TokenType.City);
                        break;
                    case "city1":
                        Spawn(row, col, TokenType.City1);
                        break;
                    case "coal":
                        Spawn(row, col, TokenType.Coal);
                        break;
                    case "fish":
                        Spawn(row, col, TokenType.Fish);
                        break;
                    case "gems":
                        Spawn(row, col, TokenType.Gems);
                        break;
                    case "grain":
                        Spawn(row, col, TokenType.Grain);
                        break;
                    case "grain1":
                        Spawn(row, col, TokenType.Grain1);
                        break;
                    case "horses":
                        Spawn(row, col, TokenType.Horses);
                        break;
                    case "ice":
                        Spawn(row, col, TokenType.Ice);
                        break;
                    case "iron":
                        Spawn(row, col, TokenType.Iron);
                        break;
                    case "stones":
                        Spawn(row, col, TokenType.Stones);
                        break;
                    case "volcano":
                        Spawn(row, col, TokenType.Volcano);
                        break;
                    case "water":
                        Spawn(row, col, TokenType.Water);
                        break;
                    case "wood":
                        Spawn(row, col, TokenType.Wood);
                        break;
                    case "gold":
                        Spawn(row, col, TokenType.Gold);
                        break;
                    case "":
                        Spawn(row, col, TokenType.None);
                        break;
                    default:
                        Spawn(row, col, TokenType.None);
                        Debug.LogError("Unknown token type: " + line[col]);
                        break;
                }
            }
        }
    }

    public Vector3 TokenCenter(int row, int col)
    {
        var cfg = AppMain.Instance.MatchParams;

        return new Vector3(
            -cfg.BoardCols * cfg.CellSize / 2 + cfg.CellSize / 2 + col * cfg.CellSize,
            cfg.BoardRows * cfg.CellSize / 2 - cfg.CellSize / 2 - row * cfg.CellSize);
    }

    void Spawn(int row, int col, TokenType tokenType)
    {
        var token = Instantiate(TokenPrefab, transform);
        token.TokenType = tokenType;
        token.Image.sprite = ResourceIcons.GetByTokenType(tokenType);

        token.transform.localPosition = TokenCenter(row, col);

        tokens[row, col] = token;
        token.showAnimationDelay = UnityEngine.Random.Range(0.5f, 1.5f);
    }

    void Update()
    {
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);

        bool overSprite = Background.bounds.Contains(mousePosition);

        if(isDragging)
        {
            touchTime += Time.deltaTime;
        }

        if (overSprite && Input.GetMouseButtonDown(0))
        {
            Stencil.OnStartDrag();
            dragStartPosition = mousePosition;
            isDragging = true;
            touchTime = 0.0f;
        }
        else if (Input.GetMouseButton(0) && isDragging)
        {
            var dragOffset = mousePosition - dragStartPosition;
            Stencil.OnDrag(dragOffset);
        } else if(!Input.GetMouseButton(0) && isDragging)
        {
            isDragging = false;
            var dragOffset = mousePosition - dragStartPosition;
            if (touchTime < 0.2f && dragOffset.magnitude < 10.0f)
            {
                Stencil.RotateShape();
            }
        }

        UpdateReign();
    }

    void UpdateReign()
    {
        reignMatrix.Make(MatchMain.Instance.PointsCalc.kingdoms);

        var matrix = reignMatrix.Matrix;

        for (int row = 0; row < tokens.GetLength(0); row++)
        {
            for (int col = 0; col < tokens.GetLength(1); col++)
            {
                var token = tokens[row, col];

                var cell = matrix[row, col];

                if (cell != null)
                {
                    token.UpdateColor(GetSelectionColor(cell.Player));
                    token.UpdateSelection(row, col, reignMatrix, cell.Player);
                }
            }
        }

        for (int row = 0; row < tokens.GetLength(0); row++)
        {
            for (int col = 0; col < tokens.GetLength(1); col++)
            {
                var curr = reignMatrix.GetCell(row, col);
                var top = reignMatrix.GetCell(row - 1, col);
                var right = reignMatrix.GetCell(row, col+1);
                var bottom = reignMatrix.GetCell(row + 1, col);
                var left = reignMatrix.GetCell(row, col - 1);

                var token = tokens[row, col];

                if(top != null && top?.Player == left?.Player && top?.Player != curr?.Player)
                {
                    token.Inverted[0].gameObject.SetActive(true);
                    token.Inverted[0].color = GetSelectionColor(top.Player);
                }
                if (right != null && top?.Player == right?.Player && right?.Player != curr?.Player)
                {
                    token.Inverted[1].gameObject.SetActive(true);
                    token.Inverted[1].color = GetSelectionColor(right.Player);
                }
                if (bottom != null && bottom?.Player == right?.Player && bottom?.Player != curr?.Player)
                {
                    token.Inverted[2].gameObject.SetActive(true);
                    token.Inverted[2].color = GetSelectionColor(bottom.Player);
                }
                if (left != null && bottom?.Player == left?.Player && left?.Player != curr?.Player)
                {
                    token.Inverted[3].gameObject.SetActive(true);
                    token.Inverted[3].color = GetSelectionColor(left.Player);
                }
            }
        }
    }

    Color GetSelectionColor(int player)
    {
        var color = MatchMain.Instance.Players[player].GetStencil().TintColor;
        return new Color(color.r, color.g, color.b, 200.0f / 255.0f);
    }

    public Token GetToken(int row, int col)
    {
        if(row < 0 || col < 0 || row >= tokens.GetLength(0) || col >= tokens.GetLength(1))
        {
            return null;
        }

        return tokens[row, col];
    }

    public void SyncSelections(Action<Token> onOccupy)
    {
        var calc = MatchMain.Instance.PointsCalc;

        foreach(var kingdom in calc.kingdoms)
        {
            for (int row =0; row < kingdom.Fields.GetLength(0); row++)
            {
                for (int col = 0; col < kingdom.Fields.GetLength(1); col++)
                {
                    if (!kingdom.Fields[row, col]) continue;

                    var token = tokens[row, col];

                    if (!token.Occupied)
                    {
                        token.Occupied = true;
                        onOccupy(token);

                        if (token.TokenType != TokenType.City && token.TokenType != TokenType.City1)
                        {
                            token.TokenType = TokenType.None;
                            token.Image.sprite = null;
                        }
                    }
                }
            }
        }
    }

    public IEnumerator SpawnNewToken()
    {
        possibleSlots.RemoveAll((possibleSlot) => GetToken(possibleSlot.row, possibleSlot.col).Occupied);

        if(possibleSlots.Count > 0)
        {
            var possibleSlot = possibleSlots[UnityEngine.Random.Range(0, possibleSlots.Count)];

            Destroy(tokens[possibleSlot.row, possibleSlot.col].gameObject);

            Spawn(possibleSlot.row, possibleSlot.col, LeastTokenType());
            tokens[possibleSlot.row, possibleSlot.col].showAnimationDelay = 0.0f;

            yield return new WaitForSeconds(1.0f);
        }
    }

    TokenType LeastTokenType()
    {
        List<TokenCount> counts = new List<TokenCount>();

        counts.Add(CountTokens(TokenType.Horses));
        counts.Add(CountTokens(TokenType.Gold));
        counts.Add(CountTokens(TokenType.Gems));
        counts.Add(CountTokens(TokenType.Iron));
        counts.Add(CountTokens(TokenType.Wood));

        int min = 999999;
        foreach(var tokenCount in counts)
        {
            if(tokenCount.Count < min)
            {
                min = tokenCount.Count;
            }
        }

        counts.RemoveAll((count) => count.Count != min);

        if(min >= 3)
        {
            List < TokenType > tokenTypes = new List<TokenType>()
            {
                TokenType.Horses,
                TokenType.Gold,
                TokenType.Gems,
                TokenType.Iron,
            };

            Debug.Log("Generate random token");
            return tokenTypes[UnityEngine.Random.Range(0, tokenTypes.Count)];
        }


        Debug.Log("Generate least token");
        return counts[UnityEngine.Random.Range(0, counts.Count)].TokenType;
    }

    TokenCount CountTokens(TokenType type)
    {
        int count = 0;

        for (int i = 0; i < tokens.GetLength(0); i++)
        {
            for (int j = 0; j < tokens.GetLength(1); j++)
            {
                if (tokens[i, j].TokenType == type && !tokens[i, j].Occupied)
                {
                    count++;
                }
            }
        }

        return new TokenCount {
            TokenType = type,
            Count = count,
        };
    }
}

public struct PossibleSlot
{
    public int row;
    public int col;
}

public struct TokenCount
{
    public TokenType TokenType;
    public int Count;
}