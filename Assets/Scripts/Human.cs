﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Human : Player
{
    public Deck Deck;
    public Stencil Stencil;
    public Button AcceptButton;
    public Button RotateButton;
    public GameObject PlacePanel;
    public Moves Moves;

    int FinishedMoves = 0;

    private void Awake()
    {
        ShowButtons(false);
    }

    private void Start()
    {
        Stencil.SetShape(Deck.Cards[0].Shape);
    }

    public override void StartMove()
    {
        ShowButtons(true);
        Stencil.SetShape(Deck.Cards[FinishedMoves].Shape);
        Stencil.CenterRowCol();
        Stencil.gameObject.SetActive(true);

        if (FinishedMoves == 7)
        {
            MatchRefs.Instance.Saying.Show("Last Move!");
        }
        else
        {
            MatchRefs.Instance.Saying.Show("Your Turn!");
        }
    }

    public override Stencil GetStencil()
    {
        return Stencil;
    }

    public float CancelMove()
    {
        float sanityDelay = Stencil.Stamp();
        ShowButtons(false);
        FinishedMoves++;
        Moves.SetFinishedMoves(FinishedMoves);
        Stencil.gameObject.SetActive(false);

        return sanityDelay;
    }

    public void AcceptMove()
    {
        if (MatchMain.Instance.MatchOver)
        {
            return;
        }

        if (MatchMain.Instance.CurrentPlayerIndex != 3)
        {
            return;
        }

        float sanityDelay = Stencil.Stamp();

        if (sanityDelay >= 0.0f)
        {
            ShowButtons(false);

            FinishedMoves++;
            Moves.SetFinishedMoves(FinishedMoves);

            MatchMain.Instance.NextPlayer(sanityDelay);
            Stencil.gameObject.SetActive(false);
        }
    }

    void ShowButtons(bool show)
    {
        PlacePanel.SetActive(show);
    }
}

