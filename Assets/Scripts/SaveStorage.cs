﻿using UnityEngine;

public class SaveStorage
{

    const string playerPrefsKey = "GameSave9";

    private static GameSave gameSave;

    public static GameSave Load()
    {
        if(gameSave == null)
        {
            if(PlayerPrefs.HasKey(playerPrefsKey))
            {
                gameSave = JsonUtility.FromJson<GameSave>(PlayerPrefs.GetString(playerPrefsKey));
            } else
            {
                gameSave = new GameSave(300, true, 0, 0);
            }
        }

        return gameSave;
    }

    public static void Save(GameSave save)
    {
        gameSave = save;

        PlayerPrefs.SetString(playerPrefsKey, JsonUtility.ToJson(gameSave));
        PlayerPrefs.Save();
    }

}
