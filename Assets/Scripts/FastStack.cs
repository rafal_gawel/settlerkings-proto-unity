﻿public class FastStack
{
    private GridIndex[] items = new GridIndex[10000];
    private int count;

    public void Push(GridIndex index)
    {
        items[count] = index;
        count++;
    }

    public GridIndex Pop()
    {
        count--;
        return items[count];
    }

    public bool IsNotEmpty()
    {
        return count != 0;
    }
}
