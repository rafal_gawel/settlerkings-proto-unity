﻿public class AppMain
{

    public static AppMain Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new AppMain();
            }

            return _instance;
        }
    }

    private static AppMain _instance;

    public MatchParams MatchParams = new MatchParams
    {
        EntryFee = 100,
        MapId = 3,
        BoardCols = 20,
        BoardRows = 10,
        CellSize = 24,
        PlayerUnitsIds = new string[] { "warior", "archer", "berserker", "sorcerer", "assasin", "knight", "maniac", "dead_king" },
        OpponentUnitsIds = new string[] { "warior", "archer", "berserker", "sorcerer", "assasin", "knight", "maniac", "dead_king" },
    };

    public OpponentParams OpponentParams = new OpponentParams
    {
        Ids = new int[]
        {
            9, 10, 11
        },
    };

    public BattleParams BattleParams = new BattleParams
        (
            playerTroops: new string[] { "warior", "berserker", "assasin", "maniac" },
            opponentTroops: new string[] { "archer", "sorcerer", "knight", "dead_king"}
        );

    public SummaryParams SummaryParams = new SummaryParams
    {
        EntryFee = 100,
        Players = new SummaryPlayer[]
        {
            new SummaryPlayer
            {
                PlayerId = 1,
                OpponentId = 11,
                Points = 91,
                Reward = 146,
            },
            new SummaryPlayer
            {
                PlayerId = 3,
                OpponentId = -1,
                Points = 60,
                Reward = 42,
            },
        },
    };
}
