﻿public class MatchParams
{
    public int EntryFee;
    public int MapId;
    public int BoardRows;
    public int BoardCols;
    public int CellSize;
    public string[] PlayerUnitsIds;
    public string[] OpponentUnitsIds;
}
