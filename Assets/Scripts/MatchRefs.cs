﻿using UnityEngine;

public class MatchRefs : MonoBehaviour
{

    public static MatchRefs Instance;

    public Canvas Canvas;
    public FlyingResource FlyingResourcePrefab;
    public GameObject FlyingResourcesParent;
    public Saying Saying;

    private void Awake()
    {
        Instance = this;
    }

}
