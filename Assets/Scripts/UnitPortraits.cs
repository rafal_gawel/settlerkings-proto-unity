﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitPortraits : MonoBehaviour
{

    public Sprite warior;
    public Sprite archer;
    public Sprite berserker;
    public Sprite sorcerer;
    public Sprite assasin;
    public Sprite knight;
    public Sprite maniac;
    public Sprite dead_king;

    public Sprite GetForId(string id)
    {
        switch(id)
        {
            case "warior": return warior;
            case "archer": return archer;
            case "berserker": return berserker;
            case "sorcerer": return sorcerer;
            case "assasin": return assasin;
            case "knight": return knight;
            case "maniac": return maniac;
            case "dead_king": return dead_king;
        }

        return null;
    }

}
