﻿using System.Collections.Generic;

public struct GridIndex
{
    public int Row;
    public int Col;
}

public class ControlledShape
{
    public List<GridIndex> Fields = new List<GridIndex>();
    public int Area;
}

public class ReignCell
{
    public int Player;
}

public class ReignMatrix
{
    public ReignCell[,] Matrix = new ReignCell[AppMain.Instance.MatchParams.BoardRows,
        AppMain.Instance.MatchParams.BoardCols];

    public List<ControlledShape>[] ControlledShapes = new List<ControlledShape>[4]
    {
        new List<ControlledShape>(),
        new List<ControlledShape>(),
        new List<ControlledShape>(),
        new List<ControlledShape>(),
    };

    public ReignCell GetCell(int row, int col)
    {
        if(row < 0 || row >= Matrix.GetLength(0) || col < 0 || col >= Matrix.GetLength(1))
        {
            return null;
        }

        return Matrix[row, col];
    }

    public void Make(PlayerKingdom[] kingdoms)
    {
        for (int row = 0; row < kingdoms[0].Fields.GetLength(0); row++)
        {
            for (int col = 0; col < kingdoms[0].Fields.GetLength(1); col++)
            {
                Matrix[row, col] = null;
            }
        }

        for (int i=0; i<kingdoms.Length; i++)
        {
            var fields = kingdoms[i].Fields;
            for(int row=0; row<fields.GetLength(0); row++)
            {
                for(int col=0; col<fields.GetLength(1); col++)
                {
                    if(fields[row, col])
                    {
                        Matrix[row, col] = new ReignCell
                        {
                            Player = i,
                        };
                    }
                }
            }
        }

        MakeControlledShapes();
    }

    void MakeControlledShapes()
    {
        foreach(var controlled in ControlledShapes)
        {
            controlled.Clear();
        }

        var fields = Matrix;
        bool[,] visited = new bool[fields.GetLength(0), fields.GetLength(1)];
        List<ControlledShape> shapes = new List<ControlledShape>();

        for (int row = 0; row < fields.GetLength(0); row++)
        {
            for (int col = 0; col < fields.GetLength(1); col++)
            {
                var field = fields[row, col];
                if (!visited[row, col] && field != null)
                {
                    ControlledShapes[field.Player].Add(MakeControlledShape(visited, field.Player, row, col));
                }
            }
        }

        foreach(var controlled in ControlledShapes)
        {
            controlled.Sort(delegate (ControlledShape x, ControlledShape y)
            {
                return y.Area.CompareTo(x.Area);
            });
        }
    }

    private static FastStack makeControlledShapeFastStack = new FastStack();

    ControlledShape MakeControlledShape(bool[,] visited, int playerId, int startRow, int startCol)
    {
        ControlledShape controlledShape = new ControlledShape();

        var opened = makeControlledShapeFastStack;

        void Open(GridIndex index)
        {
            if (!visited[index.Row, index.Col])
            {
                var field = Matrix[index.Row, index.Col];
                if (field != null && field.Player == playerId)
                {
                    opened.Push(index);
                    visited[index.Row, index.Col] = true;
                }
            }
        }

        Open(new GridIndex
        {
            Row = startRow,
            Col = startCol,
        });

        while (opened.IsNotEmpty())
        {
            var field = opened.Pop();
            controlledShape.Fields.Add(field);

            if (field.Row > 0)
            {
                Open(new GridIndex
                {
                    Row = field.Row - 1,
                    Col = field.Col,
                });
            }
            if (field.Col > 0)
            {
                Open(new GridIndex
                {
                    Row = field.Row,
                    Col = field.Col - 1,
                });
            }
            if (field.Row + 1 < Matrix.GetLength(0))
            {
                Open(new GridIndex
                {
                    Row = field.Row + 1,
                    Col = field.Col,
                });
            }
            if (field.Col + 1 < Matrix.GetLength(1))
            {
                Open(new GridIndex
                {
                    Row = field.Row,
                    Col = field.Col + 1,
                });
            }
            if (field.Row > 0 && field.Col + 1 < Matrix.GetLength(1))
            {
                Open(new GridIndex
                {
                    Row = field.Row - 1,
                    Col = field.Col + 1,
                });
            }
            if (field.Row + 1 < Matrix.GetLength(0) && field.Col > 0)
            {
                Open(new GridIndex
                {
                    Row = field.Row + 1,
                    Col = field.Col - 1,
                });
            }
            if (field.Row + 1 < Matrix.GetLength(0) && field.Col + 1 < Matrix.GetLength(1))
            {
                Open(new GridIndex
                {
                    Row = field.Row + 1,
                    Col = field.Col + 1,
                });
            }
            if (field.Row > 0 && field.Col > 0)
            {
                Open(new GridIndex
                {
                    Row = field.Row - 1,
                    Col = field.Col - 1,
                });
            }
        }

        controlledShape.Area = controlledShape.Fields.Count;

        return controlledShape;
    }
}
