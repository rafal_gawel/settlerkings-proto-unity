﻿public class SummaryParams
{
    public int EntryFee;
    public SummaryPlayer[] Players;
}

public class SummaryPlayer
{
    public int PlayerId;
    public int OpponentId;
    public int Points;
    public int Reward;
}
