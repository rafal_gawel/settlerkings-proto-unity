﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitUnlocked
{

    private static string[] rewards = new string[] {
        "maniac",
        "archer",
        "warior",
        "knight",
        "archer",
        "berserker",
        "sorcerer",
        "assasin",
        "warior",
        "sorcerer",
        "archer",
        "dead_king"
    };


    public static string LastUnlocked(int wins)
    {
        if(wins == 0)
        {
            return null;
        }

        return rewards[(wins - 1) % rewards.Length];
    }

    public static string[] Unlocked(int wins)
    {
        List<string> unlocked = new List<string>();

        void AddIfUnlocked(string id)
        {
            if(Unlocked(id, wins))
            {
                unlocked.Add(id);
            }
        }

        AddIfUnlocked("warior");
        AddIfUnlocked("archer");
        AddIfUnlocked("berserker");
        AddIfUnlocked("sorcerer");
        AddIfUnlocked("assasin");
        AddIfUnlocked("knight");
        AddIfUnlocked("maniac");
        AddIfUnlocked("dead_king");

        return unlocked.ToArray();
    }

    private static bool Unlocked(string id, int wins)
    {
        if(id == "warior" || id == "berserker" || id == "assasin")
        {
            return true;
        }

        return Array.IndexOf(rewards, id) < wins;
    }
}
