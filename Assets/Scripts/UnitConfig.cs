﻿using System.Collections.Generic;
using UnityEngine;

public struct UnitConfig
{

    public string id;

    public int damage;
    public int hp;

    public float speed;
    public float scale;

    public int wood;
    public int gems;
    public int gold;
    public int horses;
    public int iron;
    public int city;
    public int value;

    public UnitConfig(string id, int initiative, int damage,  int hp, double speed, int wood, int gems, int gold, int iron, int horses, int value, double scale)
    {
        this.id = id;

        this.damage = damage;
        this.hp = hp;

        this.speed = (float)(speed * 30);
        this.scale = (float)scale;

        this.wood = wood;
        this.gems = gems;
        this.gold = gold;
        this.horses = horses;
        this.iron = iron;
        this.city = 0;
        this.value = value;
    }


    public static readonly UnitConfig[] All = {
        new UnitConfig("warior", 0, 2, 5, 4.1, 2, 0, 0, 0, 0, 8, 0.9),
        new UnitConfig("archer", 0, 6, 6, 1.8, 0, 2, 0, 0, 0, 12, 0.95),
        new UnitConfig("berserker", 0, 4, 13, 3.8, 0, 0, 2, 0, 0, 16, 1),
        new UnitConfig("sorcerer", 0, 10, 12, 1.2, 0, 0, 0, 2, 0, 20, 1),
        new UnitConfig("knight", 0, 3, 24, 2.5, 2, 0, 2, 0, 0, 24, 1.05),
        new UnitConfig("assasin", 0, 9, 19, 3, 0, 0, 0, 0, 2, 24, 1),
        new UnitConfig("maniac", 0, 7, 22, 3.3, 3, 0, 0, 0, 1, 24, 1.1),
        new UnitConfig("dead_king", 0, 10, 24, 1, 0, 3, 0, 1, 0, 28, 1.2),
    };

    public static Dictionary<string, string> Names = new Dictionary<string, string>
        {
            { "warior", "Warrior" },
            { "archer", "Archer" },
            { "berserker", "Berserker" },
            { "sorcerer", "Sorcerer" },
            { "assasin", "Assasin" },
            { "knight", "Knight" },
            { "maniac", "Maniac" },
            { "dead_king", "Dead King" }
        };

    public static UnitConfig GetForId(string unitId)
    {
        for(int i=0; i<All.Length; i++)
        {
            if (All[i].id == unitId)
            {
                return All[i];
            }
        }

        Debug.LogError("Unit id not found " + unitId);
        return All[0];
    }

    public bool CanAfford(PlayerKingdom kingdom)
    {
        return wood <= kingdom.Wood &&
            gems <= kingdom.Gems &&
            gold <= kingdom.Gold &&
            horses <= kingdom.Horses &&
            iron <= kingdom.Iron;
    }

    public void TakeResources(PlayerKingdom kingdom)
    {
        kingdom.Wood -= wood;
        kingdom.Gems -= gems;
        kingdom.Gold -= gold;
        kingdom.Horses -= horses;
        kingdom.Iron -= iron;
    }

    public int GetCost(ResourceType resourceType)
    {
        switch (resourceType)
        {
            case ResourceType.Gems:
                return gems;
            case ResourceType.Wood:
                return wood;
            case ResourceType.Horses:
                return horses;
            case ResourceType.Iron:
                return iron;
            case ResourceType.Gold:
                return gold;
        }

        Debug.LogError("Type not handled " + resourceType);

        return 0;
    }
}
