﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class SearchOpponentPanel : MonoBehaviour
{
    public Avatars Avatars;
    public TextMeshProUGUI NickText;
    public Image PortraitImage;
    public GameObject QuestionMark;

    private void Awake()
    {
        NickText.gameObject.SetActive(false);
        PortraitImage.gameObject.SetActive(false);
        QuestionMark.gameObject.SetActive(true);
    }

    public void Setup(int index)
    {
        NickText.gameObject.SetActive(true);
        PortraitImage.gameObject.SetActive(true);
        QuestionMark.gameObject.SetActive(false);

        NickText.text = Names.Values[index];
        PortraitImage.sprite = Avatars.Sprites[index];
    }

}
