﻿using UnityEngine;

public class BotThrowConfig
{

    public static bool BotShouldThrow(int gamesPlayed)
    {
        bool[] botThrows = new bool[] {
            true,
            true,
            false,
            true,
            false,
            false,
            true,
            true,
            false,
        };

        if(gamesPlayed < botThrows.Length)
        {
            return botThrows[gamesPlayed];
        }

        return Random.Range(0, 2) == 0;
    }

}
