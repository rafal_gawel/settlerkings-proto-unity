﻿using UnityEngine;

public enum TokenType
{
    City,
    City1,
    Coal,
    Fish,
    Gems,
    Grain,
    Grain1,
    Horses,
    Ice,
    Iron,
    Stones,
    Volcano,
    Water,
    Wood,
    Gold,
    None,
}

public class Token : MonoBehaviour
{
    public SpriteRenderer Image;
    public TokenType TokenType;

    public SpriteRenderer[] Rounded;
    public SpriteRenderer[] Filled;
    public SpriteRenderer[] Inverted;

    public bool Occupied { get; set; } = false;
    public float showAnimationDelay = 0.0f;
    private float showAnimation = 0.0f;

    private void Start()
    {
        var cfg = AppMain.Instance.MatchParams;

        var scale = cfg.CellSize / 20.0f;

        transform.localScale = new Vector3(scale, scale, 1.0f);
    }

    private void Update()
    {
        showAnimationDelay -= Time.deltaTime;

        if(showAnimationDelay <= 0.0f)
        {
            showAnimation += Time.deltaTime * 2.0f;

            if (showAnimation > 1.0f)
            {
                showAnimation = 1.0f;
            }
        }

        var scale = Easing.Back.Out(showAnimation);

        Image.transform.localScale = new Vector3(scale, scale, scale);
    }

    internal void UpdateColor(Color color)
    {
        for(int i=0; i<4; i++)
        {
            Rounded[i].color = color;
            Filled[i].color = color;
        }
    }

    internal void UpdateSelection(int row, int col, ReignMatrix matrix, int player)
    {
        bool top = matrix.GetCell(row - 1, col)?.Player == player;
        bool left = matrix.GetCell(row, col - 1)?.Player == player;
        bool bottom = matrix.GetCell(row + 1, col)?.Player == player;
        bool right = matrix.GetCell(row, col + 1)?.Player == player;
        bool topLeft = matrix.GetCell(row - 1, col - 1)?.Player == player;
        bool topRight = matrix.GetCell(row - 1, col + 1)?.Player == player;
        bool bottomRight = matrix.GetCell(row + 1, col + 1)?.Player == player;
        bool bottomLeft = matrix.GetCell(row + 1, col - 1)?.Player == player;

        ActiveFilled(top || left || topLeft, 0);
        ActiveFilled(top || right || topRight, 1);
        ActiveFilled(bottom || right || bottomRight, 2);
        ActiveFilled(bottom || left || bottomLeft, 3);
    }

    void ActiveFilled(bool active, int index)
    {
        Filled[index].gameObject.SetActive(active);
        Rounded[index].gameObject.SetActive(!active);
    }
}
