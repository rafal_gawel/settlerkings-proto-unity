﻿public class BattleParams
{
    public string[] PlayerTroops { get; }
    public string[] OpponentTroops { get; }

    public BattleParams(string[] playerTroops, string[] opponentTroops)
    {
        this.PlayerTroops = playerTroops;
        this.OpponentTroops = opponentTroops;
    }
}
