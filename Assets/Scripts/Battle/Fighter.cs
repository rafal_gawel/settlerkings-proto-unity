﻿using UnityEngine;

public class Fighter : MonoBehaviour
{
    public bool IsDead { get => health == 0; }

    public Fighter Target { get; set; }
    int AttackingFightersCount = 0;
    Fighter Victim;

    public Vector2 Velocity { get; set; }

    public Color OpponentHealthColor;
    public Color PlayerHealthColor;

    public Transform ScaleTransform;
    public SpriteRenderer Sprite;
    public SpriteRenderer FlashSprite;
    public SpriteRenderer HealthBarBackground;
    public Transform HealthBar;
    public SpriteRenderer HealthBarFill;

    public UnitPortraits Portraits;
    public UnitPortraits PortraitsWhite;

    private float attackAnimation = 0.0f;
    private float flashValue = 0.0f;

    private int health;
    private UnitConfig config;
    private float alphaValue = 1.0f;

    void FixedUpdate()
    {
        if(!IsDead && BattleMain.Instance.State == BattleState.InProgress)
        {
            UpdateVictim();
            UpdateVelocity();
            UpdatePosition();
            UpdateFacing();
            UpdateAttackAnimation();
        }

        UpdateSortingOrder();
        UpdateFlash();
        UpdateHealthBar();
        UpdateAlpha();
    }

    void UpdateVictim()
    {
        if(Victim != null)
        {
            return;
        }

        if(Target == null)
        {
            return;
        }

        if ((Target.transform.position - transform.position).magnitude < 30.0f && Target.CanBeTarget())
        {
            Victim = Target;
            Victim.AttackingFightersCount++;
        }
    }

    void UpdateVelocity()
    {
        if (Victim != null)
        {
            Velocity = Vector2.zero;
        }

        Velocity *= 0.9f;

        float maxSpeed = config.speed;

        if (Velocity.magnitude > maxSpeed)
        {
            Velocity = Velocity.normalized * maxSpeed;
        }
    }

    void UpdatePosition()
    {
        Vector3 displacement = Velocity * Time.deltaTime;
        transform.localPosition += displacement;
    }

    void UpdateSortingOrder()
    {
        Sprite.sortingOrder = Mathf.RoundToInt(-transform.localPosition.y * 100 + transform.localPosition.x);
        FlashSprite.sortingOrder = Sprite.sortingOrder + 1;
    }

    void UpdateFacing()
    {
        if(Victim != null)
        {
            SetFlipX(Victim.transform.position.x < transform.position.x);
            return;
        }

        if (Sprite.flipX && Velocity.x > 1.0f)
        {
            SetFlipX(false);
        }

        if (!Sprite.flipX && Velocity.x < -1.0f)
        {
            SetFlipX(true);
        }
    }

    void UpdateAttackAnimation()
    {
        float lastValue = attackAnimation;
        attackAnimation += Time.deltaTime * 2.0f;

        if(lastValue < 0.5f && attackAnimation >= 0.5f)
        {
            if(Victim != null)
            {
                Victim.Damage(config.damage);
            }
        }

        if (attackAnimation > 1.0f)
        {
            if(Victim != null && !Victim.IsDead) {
                attackAnimation = 0.0f;
            } else
            {
                attackAnimation = 1.0f;
                Victim = null;
            }
        }

        Vector2 direction;

        if(Victim != null)
        {
            direction = (Victim.transform.position - transform.position).normalized;
        } else
        {
            direction = Vector2.zero;
        }

        Sprite.transform.localPosition = direction * 6.0f * Mathf.Sin(Mathf.PI * attackAnimation);
    }

    void UpdateFlash()
    {
        flashValue -= Time.deltaTime * 2.0f;

        if (flashValue < 0)
        {
            flashValue = 0.0f;
        }

        FlashSprite.color = new Color(1.0f, 1.0f, 1.0f, Easing.Sinusoidal.In(flashValue));

    }

    void UpdateHealthBar()
    {
        HealthBar.transform.localScale = new Vector3(health / (float)config.hp, 1.0f, 1.0f);
    }

    void UpdateAlpha()
    {
        if(!IsDead)
        {
            return;
        }

        alphaValue -= Time.deltaTime * 0.5f;
        if(alphaValue < 0.0f)
        {
            alphaValue = 0.0f;
        }

        float alpha = Easing.Sinusoidal.In(alphaValue);
        HealthBarBackground.color = new Color(0.0f, 0.0f, 0.0f, alpha);
        Sprite.color = new Color(1.0f, 1.0f, 1.0f, alpha);
    }

    internal void Setup(string troop, bool isOpponent)
    {
        config = UnitConfig.GetForId(troop);
        health = config.hp;

        Sprite.sprite = Portraits.GetForId(troop);
        FlashSprite.sprite = PortraitsWhite.GetForId(troop);
        SetFlipX(isOpponent);

        HealthBarFill.color = isOpponent ? OpponentHealthColor : PlayerHealthColor;
        var scale = config.scale;
        ScaleTransform.localScale = new Vector3(scale, scale, scale);
    }

    public bool CanBeTarget()
    {
        return AttackingFightersCount < 3 && !IsDead;
    }

    void SetFlipX(bool flipX)
    {
        Sprite.flipX = flipX;
        FlashSprite.flipX = flipX;
    }

    public void Damage(int damage)
    {
        if(IsDead)
        {
            return;
        }

        flashValue = 1.0f;

        health = Mathf.Max(0, health - damage);

        if(IsDead && Victim != null)
        {
            Victim.AttackingFightersCount--;
            Victim = null;
        }
    }
}
