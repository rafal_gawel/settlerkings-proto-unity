﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public enum BattleState
{
    NotStarted,
    InProgress,
    PlayerWon,
    OpponentWon,
    Draw
}

public class BattleMain : MonoBehaviour
{
    public static BattleMain Instance;

    public Fighter FighterPrefab;
    public Transform FighterParent;

    public Saying Saying;

    private float TimeToGoSummary = 5.0f;

    public Image OpponentPortrait;
    public Avatars Avatars;
    public TextMeshProUGUI OpponentNick;

    List<Fighter> PlayerFighters = new List<Fighter>();
    List<Fighter> OpponentFighters = new List<Fighter>();
    List<Fighter> Fighters = new List<Fighter>();

    public BattleState State { get; private set; } = BattleState.NotStarted;

    float minDistance = 20.0f;

    private void Awake()
    {
        int id = AppMain.Instance.OpponentParams.Ids[1];

        OpponentNick.text = Names.Values[id];
        OpponentPortrait.sprite = Avatars.Sprites[id];

        Instance = this;

        var battleParams = AppMain.Instance.BattleParams;

        for(int i=0; i<battleParams.PlayerTroops.Length; i++)
        {
            var position = GetStartingPosition(i, true, battleParams.PlayerTroops);
            var fighter = Instantiate(FighterPrefab, position, Quaternion.identity, FighterParent);
            fighter.Setup(battleParams.PlayerTroops[i], false);
            PlayerFighters.Add(fighter);
            Fighters.Add(fighter);
        }

        for (int i = 0; i < battleParams.OpponentTroops.Length; i++)
        {
            var position = GetStartingPosition(i, false, battleParams.OpponentTroops);
            var fighter = Instantiate(FighterPrefab, position, Quaternion.identity, FighterParent);
            fighter.Setup(battleParams.OpponentTroops[i], true);
            OpponentFighters.Add(fighter);
            Fighters.Add(fighter);
        }
    }

    private void Start()
    {
        StartCoroutine(StartMatch());
    }

    IEnumerator StartMatch()
    {
        yield return new WaitForSeconds(1.0f);

        Saying.Show("3");
        yield return new WaitForSeconds(0.5f);
        Saying.Show("2");
        yield return new WaitForSeconds(0.5f);
        Saying.Show("1");
        yield return new WaitForSeconds(0.5f);
        Saying.Show("Start!");
        yield return new WaitForSeconds(0.5f);

        State = BattleState.InProgress;
    }

    private void FixedUpdate()
    {
        for(int i=0; i< Fighters.Count; i++)
        {
            if(Fighters[i].IsDead)
            {
                continue;
            }

            for (int j = 0; j < Fighters.Count; j++)
            {
                if(i == j)
                {
                    continue;
                }

                if (Fighters[j].IsDead)
                {
                    continue;
                }

                var firstBody = Fighters[i];
                var secondBody = Fighters[j];

                Vector2 displacement = firstBody.transform.position - secondBody.transform.position;
                var distance = displacement.magnitude;
                if(distance < minDistance)
                {
                    Vector2 direction;

                    if(displacement.magnitude > 0.0f)
                    {
                        direction = displacement.normalized;
                    } else
                    {
                        direction = Vector2.up;
                    }

                    float force = (1.0f - distance / minDistance);

                    firstBody.Velocity += direction * force * 10.0f;
                    secondBody.Velocity += -direction * force * 10.0f;
                }
            }
        }

        foreach(var fighter in PlayerFighters)
        {
            FindTarget(fighter, OpponentFighters);
        }

        foreach (var fighter in OpponentFighters)
        {
            FindTarget(fighter, PlayerFighters);
        }

        FinishMatchIfNeeded();
    }

    void FinishMatchIfNeeded()
    {
        if(State != BattleState.InProgress)
        {
            return;
        }

        bool opponentDead = OpponentFighters.All((fighter) => fighter.IsDead);
        bool playerDead = PlayerFighters.All((fighter) => fighter.IsDead);

        if(opponentDead && playerDead)
        {
            State = BattleState.Draw;
        } else if(playerDead)
        {
            State = BattleState.OpponentWon;
        } else if (opponentDead)
        {
            State = BattleState.PlayerWon;
        }

        if(State != BattleState.InProgress)
        {
            StartCoroutine(FinishMatch());
        }
    }

    IEnumerator FinishMatch()
    {
        yield return new WaitForSeconds(1.0f);

        ShowSayingBasedOnBattleState();

        yield return new WaitForSeconds(2.0f);

        GoToMatchSummary();
    }

    void GoToMatchSummary()
    {
        int reward = Mathf.RoundToInt(2 * AppMain.Instance.MatchParams.EntryFee * 0.94f);

        int nonWinnerReward = State == BattleState.Draw ? reward / 2 : 0;

        var players = new List<SummaryPlayer>
        {
            new SummaryPlayer
            {
                PlayerId = 1,
                OpponentId = AppMain.Instance.OpponentParams.Ids[1],
                Points = 0,
                Reward = State == BattleState.OpponentWon ? reward : nonWinnerReward,
            },

            new SummaryPlayer
            {
                PlayerId = 3,
                OpponentId = 0,
                Points = 0,
                Reward = State == BattleState.PlayerWon ? reward : nonWinnerReward,
            }
        };

        players.Sort((a, b) => b.Reward.CompareTo(a.Reward));

        AppMain.Instance.SummaryParams = new SummaryParams
        {
            EntryFee = AppMain.Instance.MatchParams.EntryFee,
            Players = players.ToArray(),
        };

        var save = SaveStorage.Load();

        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].PlayerId == 3)
            {
                save.Coins += players[i].Reward;
            }
        }

        if (State == BattleState.PlayerWon)
        {
            save.Wins++;
        }
        save.PlayedGames++;

        SaveStorage.Save(save);


        SceneManager.LoadScene("MatchSummary");
    }

    void ShowSayingBasedOnBattleState()
    {
        Debug.LogWarning(State);

        if (State == BattleState.OpponentWon)
        {
            Saying.Show("Defeat!");
        }

        if (State == BattleState.PlayerWon)
        {
            Saying.Show("Victory!");
        }

        if (State == BattleState.Draw)
        {
            Saying.Show("Draw!");
        }

    }


    public void FindTarget(Fighter firstBody, List<Fighter> fighters)
    {
        Fighter current = null;
        var firstPosition = firstBody.transform.position;

        foreach (var secondBody in fighters)
        {
            if (!secondBody.CanBeTarget())
            {
                continue;
            }

            if (current == null)
            {
                current = secondBody;
            }
            else
            {
                var currentPosition = current.transform.position;
                var secondPosition = secondBody.transform.position;

                if (DistanceRate(currentPosition, firstPosition) > DistanceRate(secondPosition, firstPosition))
                {
                    current = secondBody;
                }
            }
        }

        if (current != null)
        {
            Vector2 displacement = current.transform.position - firstBody.transform.position;

            var distance = displacement.magnitude;

            if (distance > minDistance)
            {
                var direction = displacement.normalized;

                float force = (1.0f - distance / minDistance);

                firstBody.Velocity += -direction * force * 10.0f;
            }

            firstBody.Target = current;
        }
    }

    float DistanceRate(Vector2 first, Vector2 second)
    {
        var displacement = (first - second);

        return displacement.sqrMagnitude;
    }

    Vector2 GetStartingPosition(int index, bool humanFighter, string[] fighters)
    {
        float span = Formation.TotalHorizontalSpacing(fighters.Length);

        Vector2 position = Formation.GetDisplacement(index, humanFighter) + Formation.GetCenter(humanFighter);
        if (humanFighter)
        {
            position += new Vector2(span / 2.0f, 0.0f);
        }
        else
        {
            position -= new Vector2(span / 2.0f, 0.0f);
        }

        return position;
    }
}