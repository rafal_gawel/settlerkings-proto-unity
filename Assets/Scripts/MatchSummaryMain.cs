﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MatchSummaryMain : MonoBehaviour
{
    public TextMeshProUGUI CoinsText;
    public TextMeshProUGUI BankCoinsText;
    public SummaryRow[] Rows;

    GameSave GameSave;

    private void Start()
    {
        GameSave = SaveStorage.Load();
        UpdateCoinsText();
    }

    private void Update()
    {
        UpdateCoinsText();
        UpdateBankCoinsText();
    }

    void UpdateCoinsText()
    {
        var cfg = AppMain.Instance.SummaryParams;

        for (int i=0; i < Rows.Length; i++) {
            if(cfg.Players[i].PlayerId == 3)
            {
                // bank - fee + row.cost
                int display = GameSave.Coins - cfg.Players[i].Reward + Rows[i].GetCurrentCoins();
                CoinsText.text = display.ToString();
            }
        }
    }

    void UpdateBankCoinsText()
    {
        int total = 0;

        for(int i=0; i<Rows.Length; i++)
        {
            total += Rows[i].CoinsInBank();
        }

        BankCoinsText.text = total.ToString();
    }

    public void OnClickConfirm()
    {
        SceneManager.LoadScene("Start");
    }
}
