﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Card
{
    public string[] Shape;

    public Card(string[] shape)
    {
        this.Shape = shape;
    }

}

public class Deck : MonoBehaviour
{
    public List<Card> Cards = new List<Card>();

    private void Awake()
    {
        List<Card> AllCards = new List<Card>
        {
            new Card(
            new string[]
            {
                "     ",
                "  ## ",
                " ##  ",
                "     ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "  #  ",
                "  #  ",
                "  #  ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                " ### ",
                " # # ",
                "     ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                " ### ",
                " #   ",
                " #   ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "  #  ",
                "  #  ",
                "  #  ",
                "  #  ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                " ##  ",
                "  #  ",
                "  #  ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "     ",
                "  ## ",
                "  ## ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "  #  ",
                "  #  ",
                "  #  ",
                "  #  ",
                "  #  ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "     ",
                "  ## ",
                "  #  ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "   # ",
                " ##  ",
                "     ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "     ",
                " ##  ",
                "   # ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                " ##  ",
                "  ## ",
                "     ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "  #  ",
                "  ## ",
                "  #  ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "   # ",
                " ### ",
                " #   ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                " #   ",
                " ### ",
                "   # ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "  #  ",
                "  #  ",
                "     ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                " ##  ",
                " #   ",
                " #   ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "     ",
                "  #  ",
                "     ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "  #  ",
                " # # ",
                "     ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                " #   ",
                " # # ",
                "   # ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "     ",
                " ##  ",
                " ### ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                " ### ",
                " ##  ",
                "     ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                " ### ",
                " ### ",
                "     ",
                "     ",
            }
        ),

            new Card(
            new string[]
            {
                "     ",
                "  #  ",
                " ### ",
                "  #  ",
                "     ",
            }
        )
        };

        var mapId = AppMain.Instance.MatchParams.MapId;

        List<Card> firstCards = new List<Card>();

        int[,] firstCardsIndexes = new int[,]
        {
            {12, 11, 1, 4},
            {15, 1, 18, 8},
            {15, 1, 18, 8},
            {15, 1, 18, 6},
            {15, 8, 19, 4},
            {9, 12, 4, 16},
            {17, 9, 11, 16},
            {17, 6, 16, 4},
            {22, 18, 8, 10},
            {21, 1, 10, 6}
        };

        for (int i = 0; i < firstCardsIndexes.GetLength(1); i++)
        {
            firstCards.Add(AllCards[firstCardsIndexes[mapId, i]]);
        }

        Shuffle(firstCards);

        List<Card> lastCards = new List<Card>();

        int[,] lastCardsIndexes = new int[,]
        {
            {15, 8, 0, 17},
            {9, 10, 12, 11},
            {9, 10, 12, 11},
            {5, 23, 13, 2},
            {5, 0, 14, 7},
            {3, 13, 23, 20},
            {3, 2, 7, 20},
            {19, 5, 7, 2},
            {19, 0, 14, 22},
            {3, 0, 21, 13}
        };

        for (int i = 0; i < lastCardsIndexes.GetLength(1); i++)
        {
            lastCards.Add(AllCards[lastCardsIndexes[mapId, i]]);
        }

        Shuffle(lastCards);

        Cards.AddRange(firstCards);
        Cards.AddRange(lastCards);
    }

    static void Shuffle(List<Card> cards)
    {
        for (int i = 0; i < cards.Count; i++)
        {
            var temp = cards[i];
            int randomIndex = Random.Range(i, cards.Count);
            cards[i] = cards[randomIndex];
            cards[randomIndex] = temp;
        }
    }

}
