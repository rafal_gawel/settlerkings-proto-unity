﻿using UnityEngine;

public class Trophy : MonoBehaviour
{
    void Start()
    {
        var players = AppMain.Instance.SummaryParams.Players;

        gameObject.SetActive(players[0].Reward != players[1].Reward);
    }

}
