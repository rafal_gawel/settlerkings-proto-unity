﻿using System.Collections.Generic;

public class PlayerTroops
{
    public Dictionary<string, int> troops = new Dictionary<string, int>();

    public void Add(string id)
    {
        if(!troops.ContainsKey(id))
        {
            troops[id] = 0;
        }

        troops[id] += 1;
    }

    public int Count(string id)
    {
        if (!troops.ContainsKey(id))
        {
            return 0;
        }

        return troops[id];
    }

    public int TotalValue()
    {
        int value = 0;

        foreach (var troop in troops)
        {
            var cfg = UnitConfig.GetForId(troop.Key);

            value += cfg.value * troop.Value;
        }

        return value;
    }

    public PlayerTroops Clone()
    {
        PlayerTroops cloned = new PlayerTroops();

        foreach (var troop in troops)
        {
            cloned.troops[troop.Key] = troop.Value;
        }

        return cloned;
    }
}
