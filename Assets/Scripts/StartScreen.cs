﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScreen : MonoBehaviour
{
    public void OnClickStart()
    {
        SceneManager.LoadScene("Levels");
    }
}
