﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UnitCell : MonoBehaviour
{
    public ResPrice ResPricePrefab;
    public Transform ResPriceParent;

    public UnitPortraits Portraits;
    public ResourceIcons ResourceIcons;

    public TextMeshProUGUI NameText;
    public Image Portrait;
    public TextMeshProUGUI DamageText;
    public TextMeshProUGUI HealthText;

    public void Setup(string unitId)
    {
        var config = UnitConfig.GetForId(unitId);

        NameText.text = UnitConfig.Names[unitId];
        Portrait.sprite = Portraits.GetForId(unitId);
        DamageText.text = config.damage.ToString();
        HealthText.text = config.hp.ToString();

        AddPriceIfNeeded(config.wood, "tree");
        AddPriceIfNeeded(config.gems, "gems");
        AddPriceIfNeeded(config.gold, "gold");
        AddPriceIfNeeded(config.horses, "horses");
        AddPriceIfNeeded(config.iron, "iron");
        AddPriceIfNeeded(config.city, "city");
    }

    void AddPriceIfNeeded(int price, string id)
    {
        if (price <= 0)
        {
            return;
        }

        var resPrice = Instantiate(ResPricePrefab, ResPriceParent);

        resPrice.Icon.sprite = ResourceIcons.GetById(id);
        resPrice.Text.text = price.ToString();

        resPrice.Price = price;
        resPrice.ResourceType = ResourceTypeConv.FromString(id);

        switch (id)
        {
            case "tree":
                resPrice.TokenType = TokenType.Wood;
                break;
            case "gems":
                resPrice.TokenType = TokenType.Gems;
                break;
            case "gold":
                resPrice.TokenType = TokenType.Gold;
                break;
            case "horses":
                resPrice.TokenType = TokenType.Horses;
                break;
            case "iron":
                resPrice.TokenType = TokenType.Iron;
                break;
            case "city":
                resPrice.TokenType = TokenType.City;
                break;
        }
    }
}
