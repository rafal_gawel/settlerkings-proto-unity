﻿using System;
using UnityEngine;

public class FlyingResource : MonoBehaviour
{
    public Action OnFinished;
    public SpriteRenderer Sprite;

    public float animationDelay { get; set; }

    float disappearAnimation = 1.0f;

    void Update()
    {
        if(animationDelay > 0.0f)
        {
            animationDelay -= Time.deltaTime;
            return;
        }

        disappearAnimation -= Time.deltaTime * 3.0f;

        var scale = 0.3f + 0.7f * Easing.Back.Out(disappearAnimation);

        disappearAnimation = Mathf.Max(0.0f, disappearAnimation);

        transform.localScale = new Vector3(scale, scale, scale);

        if (disappearAnimation <= 0.0f)
        {
            Destroy(gameObject);
            OnFinished();
            return;
        }
    }
}
