﻿using UnityEngine;

public enum ResourceType
{
    Gems,
    Wood,
    Horses,
    Iron,
    Gold,
}

class ResourceTypeConv
{
    public static ResourceType FromString(string id)
    {
        switch (id)
        {
            case "gems":
                return ResourceType.Gems;
            case "tree":
                return ResourceType.Wood;
            case "horses":
                return ResourceType.Horses;
            case "iron":
                return ResourceType.Iron;
            case "gold":
                return ResourceType.Gold;
        }

        Debug.Log("Not handled " + id);

        return ResourceType.Wood;
    }
}


public class PointsCalc
{

    public PlayerKingdom[] kingdoms = new PlayerKingdom[4]
    {
        new PlayerKingdom(),
        new PlayerKingdom(),
        new PlayerKingdom(),
        new PlayerKingdom(),
    };

    private ReignMatrix reignMatrix = new ReignMatrix();
    private bool reignMatrixSynced = false;

    public void AddKingdomField(int playerIndex, int row, int col)
    {
        var kingdom = kingdoms[playerIndex];
        kingdom.Fields[row, col] = true;
        reignMatrixSynced = false;
    }

    public void AddToKingdom(int playerIndex, TokenType tokenType)
    {
        var kingdom = kingdoms[playerIndex];

        kingdom.Add(tokenType);
    }

    static FastStack makeFillFastStack = new FastStack();

    public void FillShapes(int playerId)
    {
        int rows = AppMain.Instance.MatchParams.BoardRows;
        int cols = AppMain.Instance.MatchParams.BoardCols;

        bool[,] fields = kingdoms[playerId].Fields;
        bool[,] outside = new bool[rows, cols];
        bool[,] visited = new bool[rows, cols];

        var opened = makeFillFastStack;

        void Open(GridIndex index)
        {
            if (!visited[index.Row, index.Col] && !fields[index.Row, index.Col])
            {
                opened.Push(index);
                visited[index.Row, index.Col] = true;
            }
        }

        for (int row = 0; row < rows; row++)
        {
            Open(new GridIndex
            {
                Row = row,
                Col = 0,
            });
            Open(new GridIndex
            {
                Row = row,
                Col = cols - 1,
            });
        }

        for (int col = 0; col < cols; col++)
        {
            Open(new GridIndex
            {
                Row = 0,
                Col = col,
            });


            Open(new GridIndex
            {
                Row = rows - 1,
                Col = col,
            });
        }

        while (opened.IsNotEmpty())
        {
            var field = opened.Pop();
            outside[field.Row, field.Col] = true;

            if (field.Row > 0)
            {
                Open(new GridIndex
                {
                    Row = field.Row - 1,
                    Col = field.Col,
                });
            }
            if (field.Col > 0)
            {
                Open(new GridIndex
                {
                    Row = field.Row,
                    Col = field.Col - 1,
                });
            }
            if (field.Row + 1 < fields.GetLength(0))
            {
                Open(new GridIndex
                {
                    Row = field.Row + 1,
                    Col = field.Col,
                });
            }
            if (field.Col + 1 < fields.GetLength(1))
            {
                Open(new GridIndex
                {
                    Row = field.Row,
                    Col = field.Col + 1,
                });
            }
        }

        for (int row = 0; row < fields.GetLength(0); row++)
        {
            for (int col = 0; col < fields.GetLength(1); col++)
            {
                if (!fields[row, col] && !outside[row, col])
                {
                    bool empty = true;
                    for(int i=0; i<kingdoms.Length; i++)
                    {
                        if(kingdoms[i].Fields[row, col])
                        {
                            empty = false;
                            break;
                        }
                    }

                    if(empty)
                    {
                        fields[row, col] = true;
                    }
                }
            }
        }
    }

    public int GetKingdomPoints(int playerIndex, ResourceScoring scoring)
    {
        var kingdom = kingdoms[playerIndex];

        int points = 0;

        points += kingdom.Wood * scoring.Wood;
        points += kingdom.Gems * scoring.Gems;
        points += kingdom.Gold * scoring.Gold;
        points += kingdom.Iron * scoring.Iron;
        points += kingdom.Horses * scoring.Horses;

        return points;
    }

    public PlayerKingdom GetKingdom(int playerIndex)
    {
        return kingdoms[playerIndex];
    }

    public int LargestAreaPoints(int playerIndex)
    {
        var area = LargestArea(playerIndex);
        return area?.Area ?? 0;
    }

    public ControlledShape LargestArea(int playerIndex)
    {
        if (!reignMatrixSynced)
        {
            reignMatrixSynced = true;
            reignMatrix.Make(kingdoms);
        }

        if (reignMatrix.ControlledShapes[playerIndex].Count > 0)
        {
            return reignMatrix.ControlledShapes[playerIndex][0];
        }
        else
        {
            return null;
        }
    }

    public void Clone(PointsCalc pointsCalc)
    {
        pointsCalc.reignMatrixSynced = false;
        kingdoms[0].Clone(pointsCalc.kingdoms[0]);
        kingdoms[1].Clone(pointsCalc.kingdoms[1]);
        kingdoms[2].Clone(pointsCalc.kingdoms[2]);
        kingdoms[3].Clone(pointsCalc.kingdoms[3]);
    }

}

public class PlayerKingdom
{

    public int Gems = 0;
    public int Wood = 0;
    public int Horses = 0;
    public int Ice = 0;
    public int Iron = 0;
    public int City = 0;
    public int City1 = 0;
    public int Grain;
    public int Grain1 = 0;
    public int Water = 0;
    public int Fish = 0;
    public int Stones = 0;
    public int Volcano = 0;
    public int Coal = 0;
    public int Gold = 0;
    public bool[,] Fields = new bool[AppMain.Instance.MatchParams.BoardRows,
        AppMain.Instance.MatchParams.BoardCols];

    public void Clone(PlayerKingdom destination)
    {
        for (int i = 0; i < Fields.GetLength(0); i++)
        {
            for (int j = 0; j < Fields.GetLength(1); j++)
            {
                destination.Fields[i, j] = Fields[i, j];
            }
        }


        destination.Gems = Gems;
        destination.Wood = Wood;
        destination.Horses = Horses;
        destination.Ice = Ice;
        destination.Iron = Iron;
        destination.City = City;
        destination.City1 = City1;
        destination.Grain = Grain;
        destination.Grain1 = Grain1;
        destination.Water = Water;
        destination.Fish = Fish;
        destination.Stones = Stones;
        destination.Volcano = Volcano;
        destination.Coal = Coal;
        destination.Gold = Gold;
    }

    public void Add(TokenType tokenType)
    {
        switch (tokenType)
        {
            case TokenType.Gems: Gems++; break;
            case TokenType.Wood: Wood++; break;
            case TokenType.Horses: Horses++; break;
            case TokenType.Ice: Ice++; break;
            case TokenType.Iron: Iron++; break;
            case TokenType.City: City++; break;
            case TokenType.City1: City1++; break;
            case TokenType.Grain: Grain++; break;
            case TokenType.Grain1: Grain1++; break;
            case TokenType.Water: Water++; break;
            case TokenType.Fish: Fish++; break;
            case TokenType.Stones: Stones++; break;
            case TokenType.Volcano: Volcano++; break;
            case TokenType.Coal: Coal++; break;
            case TokenType.Gold: Gold++; break;
        }
    }

    public int GetAmount(ResourceType resourceType)
    {
        switch(resourceType)
        {
            case ResourceType.Gems:
                return Gems;
            case ResourceType.Wood:
                return Wood;
            case ResourceType.Horses:
                return Horses;
            case ResourceType.Iron:
                return Iron;
            case ResourceType.Gold:
                return Gold;
        }

        Debug.LogError("Not handled " + resourceType);

        return 0;
    }

}

public struct ResourceScoring
{
    public int Wood;
    public int Gems;
    public int Gold;
    public int Iron;
    public int Horses;
}