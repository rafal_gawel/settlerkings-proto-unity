﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ResPrice : MonoBehaviour
{
    public Image Icon;
    public TextMeshProUGUI Text;

    public int Price { get; set; }
    public ResourceType ResourceType { get; set; }
    public TokenType TokenType { get; set; }
}
