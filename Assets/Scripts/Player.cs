﻿using UnityEngine;

public abstract class Player : MonoBehaviour
{
    public abstract Stencil GetStencil();
    public abstract void StartMove();
}
