﻿using UnityEngine;

public class Formation
{

    const float HORIZONTAL_SPACING = 16.0f;
    const float VERTICAL_SPACING = 12.0f;

    static readonly int[,] formation = new int[,]
    {
        {31,-1,18,-1,9},
        {-1,24,-1,10,-1},
        {29,-1,16,-1,5},
        {-1,22,-1,6,-1},
        {27,-1,14,-1,1},
        {-1,20,-1,2,-1},
        {26,-1,13,-1,0},
        {-1,21,-1,4,-1},
        {28,-1,15,-1,3},
        {-1,23,-1,8,-1},
        {30,-1,17,-1,7},
        {-1,25,-1,12,-1},
        {32,-1,19,-1,11},
    };

    public static Vector2 GetDisplacement(int index, bool humanFighter)
    {
        index %= 33;

        int negativeX = humanFighter ? -1 : 1;

        for(int row = 0; row < formation.GetLength(0); row++)
        {
            for (int col = 0; col < formation.GetLength(1); col++)
            {
                if(formation[row, col] == index)
                {
                    return new Vector2(negativeX * (formation.GetLength(1) - 1 - col) * HORIZONTAL_SPACING,
                        (formation.GetLength(0) / 2 - row) * VERTICAL_SPACING);
                }
            }
        }

        Debug.LogError("No entry in formation!");

        return Vector2.zero;
    }

    public static Vector2 GetCenter(bool humanFighter)
    {
        if(humanFighter)
        {
            return new Vector2(-294.0f, -32.0f);
        } else
        {
            return new Vector2(294.0f, -32.0f);
        }
    }

    public static float TotalHorizontalSpacing(int count)
    {
        if(count <= 2)
        {
            return HORIZONTAL_SPACING;
        } else if(count <= 13)
        {
            return HORIZONTAL_SPACING * 2.0f;
        } else if(count <= 20)
        {
            return HORIZONTAL_SPACING * 3.0f;
        } else if(count <= 26)
        {
            return HORIZONTAL_SPACING * 4.0f;
        } else
        {
            return HORIZONTAL_SPACING * 5.0f;
        }
    }
}
